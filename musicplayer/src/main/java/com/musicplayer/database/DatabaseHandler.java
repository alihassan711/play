package com.musicplayer.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.musicplayer.models.Alarm;
import com.musicplayer.models.CustomQ;
import com.musicplayer.models.SegmentedTrack;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;

import java.util.ArrayList;

/**
 * Created by umair on 11/5/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static DatabaseHandler sharedInstance;

    public static DatabaseHandler getSharedInstance(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new DatabaseHandler(context);
        }
        return sharedInstance;
    }


    private DatabaseHandler(Context context) {
        super(context, Constants.DATABASE.DB_NAME, null, Constants.DATABASE.DB_VERSION);
        getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTrackTable(db);
        createSegmentedTrackTable(db);
        createCustomQTable(db);
        createCustomQTypeTable(db);
        createCustomQTracksTable(db);
//        saveCustomQTypes(db);
        createAlarmTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTrackTable(db);
        dropSegmentedTrackTable(db);
        dropCustomQTable(db);
        dropCustomQTypeTable(db);
        dropCustomQTracksTable(db);
        dropAlarmTable(db);
        createTrackTable(db);
        createSegmentedTrackTable(db);
        createCustomQTable(db);
        createCustomQTypeTable(db);
        createCustomQTracksTable(db);
//        saveCustomQTypes(db);
        createAlarmTable(db);
    }

    //Track Table Methods
    private void createTrackTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(" integer primary key, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.DURATION).append(" integer, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YEAR).append(" integer, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.PLAY_COUNT).append(" integer, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.CREATED_AT).append(" integer, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.LAST_PLAYED).append(" integer, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.STORAGE_ID).append(" integer, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.TITLE).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.STORAGE_PATH).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_TITLE).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_THUMB_URL).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_VIDEO_URL).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.THUMB_IMG_URL).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.THUMB_IMG_STORAGE_PATH).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.FULL_IMG_URL).append(" text, ")
                .append(Constants.DATABASE.TRACK.COLUMNS.FULL_IMG_STORAGE_PATH).append(" text ")
                .append(")");
        db.execSQL(builder.toString());
    }

    private void dropTrackTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("drop table ").append(Constants.DATABASE.TRACK.TABLE_NAME);
        db.execSQL(builder.toString());
    }

    private void createSegmentedTrackTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(Constants.DATABASE.SEGMENTED_TRACK.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.TRACK_ID).append(" integer, ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.START_TIME).append(" integer, ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.END_TIME).append(" integer ")
                .append(")");
        db.execSQL(builder.toString());
    }

    private void dropSegmentedTrackTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("drop table ").append(Constants.DATABASE.SEGMENTED_TRACK.TABLE_NAME);
        db.execSQL(builder.toString());
    }

    public void saveTrack(Track track) {

        try {

            SQLiteDatabase db = getWritableDatabase();
            String query = new StringBuilder()
                    .append("insert into ")
                    .append(Constants.DATABASE.TRACK.TABLE_NAME)
                    .append(" (")
                    .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.DURATION).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.YEAR).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.PLAY_COUNT).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.CREATED_AT).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.LAST_PLAYED).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.STORAGE_ID).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.TITLE).append(", ")
                    .append(Constants.DATABASE.TRACK.COLUMNS.STORAGE_PATH).append(") values (")
                    .append(track.getId()).append(", ")
                    .append(track.getDuration()).append(", ")
                    .append(track.getYear()).append(", ")
                    .append(track.getPlayCount()).append(", ")
                    .append(track.getCreatedAt()).append(", ")
                    .append(track.getLastPlayed()).append(", ")
                    .append(track.getStorageId()).append(", '")
                    .append(track.getTitle().replace("'", "''")).append("', '")
                    .append(track.getStoragePath().replace("'", "''")).append("') ")
                    .toString();
            db.execSQL(query);

        }catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public ArrayList<Track> loadTracks() {

        ArrayList<Track> tracks = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("select ")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.DURATION).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YEAR).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.PLAY_COUNT).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.CREATED_AT).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.LAST_PLAYED).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.STORAGE_ID).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.TITLE).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.STORAGE_PATH).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_TITLE).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_THUMB_URL).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_VIDEO_URL).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.THUMB_IMG_URL).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.THUMB_IMG_STORAGE_PATH).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.FULL_IMG_URL).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.FULL_IMG_STORAGE_PATH).append(" from ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(";")
                .toString();
        Cursor result = db.rawQuery(query, null);
        if (result == null) {
            return tracks;
        }
        while (result.moveToNext()) {
            Track track = new Track();
            track.setId(result.getLong(0));
            track.setDuration(result.getLong(1));
            track.setYear(result.getLong(2));
            track.setPlayCount(result.getLong(3));
            track.setCreatedAt(result.getLong(4));
            track.setLastPlayed(result.getLong(5));
            track.setStorageId(result.getInt(6));
            track.setTitle(result.getString(7));
            track.setStoragePath(result.getString(8));
            track.setYoutubeTitle(result.getString(9));
            track.setYoutubeThumbUrl(result.getString(10));
            track.setYoutubeVideoUrl(result.getString(11));
            track.setThumbImgUrl(result.getString(12));
            track.setThumbImgStoragePath(result.getString(13));
            track.setFullImgUrl(result.getString(14));
            track.setFullImgStoragePath(result.getString(15));
            tracks.add(track);
        }
        result.close();
        return tracks;

    }

    public void saveSegmentedTrack(Track track, long startTime, long endTime) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("insert into ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.TRACK_ID).append(", ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.START_TIME).append(", ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.END_TIME).append(") values (")
                .append(track.getId()).append(", ")
                .append(startTime).append(", ")
                .append(endTime).append("); ")
                .toString();
        db.execSQL(query);

    }

    public ArrayList<SegmentedTrack> loadSegmentedTracks(ArrayList<Track> tracks) {

        ArrayList<SegmentedTrack> segmentedTracks = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("select ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.TRACK_ID).append(", ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.START_TIME).append(", ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.COLUMNS.END_TIME).append(" from ")
                .append(Constants.DATABASE.SEGMENTED_TRACK.TABLE_NAME)
                .append(";")
                .toString();
        Cursor result = db.rawQuery(query, null);
        if (result == null) {
            return segmentedTracks;
        }
        while (result.moveToNext()) {
            long trackId = result.getLong(0);
            long startTime = result.getLong(1);
            long endTime = result.getLong(2);

            Track track = null;
            for (Track tr : tracks) {
                if (tr.getId() == trackId) {
                    track = tr;
                    break;
                }
            }
            if (track != null) {
                SegmentedTrack segmentedTrack = new SegmentedTrack(track, startTime, endTime);
                segmentedTracks.add(segmentedTrack);
            }

        }
        result.close();
        return segmentedTracks;

    }

    //CustomQ Table Methods
    private void createCustomQTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(" integer primary key, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.PLAY_COUNT).append(" integer, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.CREATED_AT).append(" integer, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.TITLE).append(" text, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.STORAGE_ID).append(" integer, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.CUSTOM_Q_TYPE_ID).append(" integer, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.THUMB_IMG_URL).append(" text, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.THUMB_IMG_STORAGE_PATH).append(" text, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.FULL_IMG_URL).append(" text, ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.FULL_IMG_STORAGE_PATH).append(" text ")
                .append(")");
        db.execSQL(builder.toString());
    }

    private void dropCustomQTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("drop table ").append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME);
        db.execSQL(builder.toString());
    }

    private void createCustomQTypeTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(Constants.DATABASE.CUSTOM_Q_TYPE.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.CUSTOM_Q_TYPE.COLUMNS.ID).append(" integer primary key, ")
                .append(Constants.DATABASE.CUSTOM_Q_TYPE.COLUMNS.TITLE).append(" text ")
                .append(")");
        db.execSQL(builder.toString());
    }

    private void dropCustomQTypeTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("drop table ").append(Constants.DATABASE.CUSTOM_Q_TYPE.TABLE_NAME);
        db.execSQL(builder.toString());
    }

    private void createCustomQTracksTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.CUSTOM_Q_ID).append(" integer, ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.TRACK_ID).append(" integer ")
                .append(")");
        db.execSQL(builder.toString());

    }

    private void dropCustomQTracksTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("drop table ").append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME);
        db.execSQL(builder.toString());
    }

    private void saveCustomQTypes(SQLiteDatabase db) {

        Constants.DATABASE.CUSTOM_Q_TYPES.put("MyQ", 1);
        Constants.DATABASE.CUSTOM_Q_TYPES.put("Artist", 2);
        Constants.DATABASE.CUSTOM_Q_TYPES.put("Genre", 3);
        Constants.DATABASE.CUSTOM_Q_TYPES.put("Album", 4);
        Constants.DATABASE.CUSTOM_Q_TYPES.put("Mood", 5);
        Constants.DATABASE.CUSTOM_Q_TYPES.put("Playlist", 6);

        for (String title : Constants.DATABASE.CUSTOM_Q_TYPES.keySet()) {
            String query = new StringBuilder()
                    .append("insert into ")
                    .append(Constants.DATABASE.CUSTOM_Q_TYPE.TABLE_NAME)
                    .append(" (")
                    .append(Constants.DATABASE.CUSTOM_Q_TYPE.COLUMNS.ID).append(", ")
                    .append(Constants.DATABASE.CUSTOM_Q_TYPE.COLUMNS.TITLE).append(") values (")
                    .append(Constants.DATABASE.CUSTOM_Q_TYPES.get(title).intValue()).append(", '")
                    .append(title).append("')").toString();
            db.execSQL(query);
        }

    }

    public void saveCustomQ(CustomQ customQ, int customQTypeId) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("insert into ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.PLAY_COUNT).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.CREATED_AT).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.TITLE).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.STORAGE_ID).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.CUSTOM_Q_TYPE_ID).append(") values (")
                .append(customQ.getId()).append(", ")
                .append(customQ.getPlayCount()).append(", ")
                .append(customQ.getCreatedAt()).append(", '")
                .append(customQ.getTitle().replace("'","''")).append("', ")
                .append(customQ.getStorageId()).append(", ")
                .append(customQTypeId).append(") ")
                .toString();
        db.execSQL(query);

        for(Track track : customQ.getAllTracks()) {
            query = new StringBuilder()
                    .append("insert into ")
                    .append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                    .append(" (")
                    .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.CUSTOM_Q_ID).append(", ")
                    .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.TRACK_ID).append(") values (")
                    .append(customQ.getId()).append(", ")
                    .append(track.getId()).append(") ")
                    .toString();
            db.execSQL(query);
        }

    }

    public void loadCustomQTypes() {

        SQLiteDatabase database = getWritableDatabase();
        String query = new StringBuilder()
                .append("select ")
                .append(Constants.DATABASE.CUSTOM_Q_TYPE.COLUMNS.ID).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q_TYPE.COLUMNS.TITLE).append(" from ")
                .append(Constants.DATABASE.CUSTOM_Q_TYPE.TABLE_NAME)
                .append(";")
                .toString();
        Cursor result = database.rawQuery(query, null);
        if (result == null) {
            return;
        }
        while (result.moveToNext()) {
            Constants.DATABASE.CUSTOM_Q_TYPES.put(result.getString(1), result.getInt(0));
        }
        result.close();

    }

    public ArrayList<CustomQ> loadCustomQs(int customQTypeId, ArrayList<Track> tracks) {

        ArrayList<CustomQ> customQs = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("select ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.PLAY_COUNT).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.CREATED_AT).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.TITLE).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.STORAGE_ID).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.THUMB_IMG_URL).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.THUMB_IMG_STORAGE_PATH).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.FULL_IMG_URL).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.FULL_IMG_STORAGE_PATH).append(" from ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.CUSTOM_Q_TYPE_ID)
                .append(" = ")
                .append(customQTypeId)
                .append(";")
                .toString();
        Cursor result = db.rawQuery(query, null);
        if (result == null) {
            return customQs;
        }
        while (result.moveToNext()) {

            CustomQ customQ = new CustomQ();
            customQ.setId(result.getLong(0));
            customQ.setPlayCount(result.getLong(1));
            customQ.setCreatedAt(result.getLong(2));
            customQ.setTitle(result.getString(3));
            customQ.setStorageId(result.getLong(4));
            customQ.setThumbImgUrl(result.getString(5));
            customQ.setThumbImgStoragePath(result.getString(6));
            customQ.setFullImgUrl(result.getString(7));
            customQ.setFullImgStoragePath(result.getString(8));

            query = new StringBuilder()
                    .append("select ")
                    .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.TRACK_ID)
                    .append(" from ")
                    .append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                    .append(" where ")
                    .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.CUSTOM_Q_ID)
                    .append(" = ")
                    .append(customQ.getId())
                    .append(";")
                    .toString();
            Cursor trackIds = db.rawQuery(query, null);
            if (trackIds != null) {
                while (trackIds.moveToNext()) {
                    Track track = getTrackWithId(tracks, trackIds.getLong(0));
                    if (track != null) {
                        customQ.addTrackWithoutDuplicateCheck(track);
                    }
                }
                trackIds.close();
            }
            customQs.add(customQ);
        }
        result.close();
        return customQs;

    }

    private Track getTrackWithId(ArrayList<Track> tracks, long trackId) {
        for (Track track : tracks) {
            if (track.getId() == trackId) {
                return track;
            }
        }
        return null;
    }

    //Methods for update
    public void addTrack(CustomQ customQ, Track track) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("insert into ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.CUSTOM_Q_ID).append(", ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.TRACK_ID).append(") values (")
                .append(customQ.getId()).append(", ")
                .append(track.getId()).append(")")
                .toString();
        db.execSQL(query);

    }

    public void deleteTrack(Track track) {
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("delete from ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.TRACK_ID).append(" = ")
                .append(track.getId()).append(";")
                .toString();
        db.execSQL(query);

        query = new StringBuilder()
                .append("delete from ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(" = ")
                .append(track.getId()).append(";")
                .toString();
        db.execSQL(query);

    }

    public void deleteCustomQ(CustomQ customQ) {
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("delete from ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.CUSTOM_Q_ID).append(" = ")
                .append(customQ.getId()).append(";")
                .toString();
        db.execSQL(query);

        query = new StringBuilder()
                .append("delete from ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(" = ")
                .append(customQ.getId()).append(";")
                .toString();
        db.execSQL(query);

    }

    public void deleteTrackFromCustomQ(CustomQ customQ, Track track) {
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("delete from ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.CUSTOM_Q_ID).append(" = ")
                .append(customQ.getId()).append(" and ")
                .append(Constants.DATABASE.CUSTOM_Q_TRACKS.COLUMNS.TRACK_ID).append(" = ")
                .append(track.getId()).append(";")
                .toString();
        db.execSQL(query);
    }

    public void changeTitleCustomQ(CustomQ customQ) {
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("update ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.TITLE).append(" = '")
                .append(customQ.getTitle()).append("' where ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(" = ")
                .append(customQ.getId()).append(";")
                .toString();
        db.execSQL(query);
    }

    public void updatePlayCountAndTime(Track track) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("update ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.TRACK.COLUMNS.PLAY_COUNT).append(" = ")
                .append(track.getPlayCount()).append(", ")
                .append(Constants.DATABASE.TRACK.COLUMNS.LAST_PLAYED).append(" = ")
                .append(track.getLastPlayed()).append(" where ")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(" = ")
                .append(track.getId()).append(";")
                .toString();
        db.execSQL(query);

    }

    public void updatePlayCount(CustomQ customQ) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("update ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.PLAY_COUNT).append(" = ")
                .append(customQ.getPlayCount()).append(" where ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(" = ")
                .append(customQ.getId()).append(";")
                .toString();
        db.execSQL(query);

    }

    public int getNextTrackId() {

        int nextTrackId = 1;
        SQLiteDatabase database = getWritableDatabase();
        String query = new StringBuilder()
                .append("select max(")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(") + 1 as next_id from ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(";")
                .toString();
        Cursor result = database.rawQuery(query, null);
        if (result == null) {
            return nextTrackId;
        }
        if (result.moveToNext()) {
            nextTrackId = result.getInt(0);
        }
        result.close();
        return nextTrackId;
    }

    public int getNextCustomQId() {

        int nextTrackId = 1;
        SQLiteDatabase database = getWritableDatabase();
        String query = new StringBuilder()
                .append("select max(")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(") + 1 as next_id from ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(";")
                .toString();
        Cursor result = database.rawQuery(query, null);
        if (result == null) {
            return nextTrackId;
        }
        if (result.moveToNext()) {
            nextTrackId = result.getInt(0);
        }
        result.close();
        return nextTrackId;
    }

    public void updateYoutubeData(Track track) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" update ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_TITLE).append(" = '")
                .append(track.getYoutubeTitle().replace("'","''")).append("', ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_THUMB_URL).append(" = '")
                .append(track.getYoutubeThumbUrl().replace("'","''")).append("', ")
                .append(Constants.DATABASE.TRACK.COLUMNS.YOUTUBE_VIDEO_URL).append(" = '")
                .append(track.getYoutubeVideoUrl().replace("'","''")).append("' where ")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(" = ")
                .append(track.getId()).append("; ")
                .toString();
        db.execSQL(query);

    }

    public void updateTrackThumbImgData(Track track) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" update ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.TRACK.COLUMNS.THUMB_IMG_URL).append(" = '")
                .append(track.getThumbImgUrl().replace("'","''")).append("', ")
                .append(Constants.DATABASE.TRACK.COLUMNS.THUMB_IMG_STORAGE_PATH).append(" = '")
                .append(track.getThumbImgStoragePath().replace("'","''")).append("' where ")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(" = ")
                .append(track.getId()).append("; ")
                .toString();
        db.execSQL(query);

    }

    public void updateTrackFullImgData(Track track) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" update ")
                .append(Constants.DATABASE.TRACK.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.TRACK.COLUMNS.FULL_IMG_URL).append(" = '")
                .append(track.getFullImgUrl().replace("'","''")).append("', ")
                .append(Constants.DATABASE.TRACK.COLUMNS.FULL_IMG_STORAGE_PATH).append(" = '")
                .append(track.getFullImgStoragePath().replace("'","''")).append("' where ")
                .append(Constants.DATABASE.TRACK.COLUMNS.ID).append(" = ")
                .append(track.getId()).append("; ")
                .toString();
        db.execSQL(query);

    }

    public void updateCustomQThumbImgData(CustomQ customQ) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" update ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.THUMB_IMG_URL).append(" = '")
                .append(customQ.getThumbImgUrl().replace("'","''")).append("', ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.THUMB_IMG_STORAGE_PATH).append(" = '")
                .append(customQ.getThumbImgStoragePath().replace("'","''")).append("' where ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(" = ")
                .append(customQ.getId()).append("; ")
                .toString();
        db.execSQL(query);

    }

    public void updateCustomQFullImgData(CustomQ customQ) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" update ")
                .append(Constants.DATABASE.CUSTOM_Q.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.FULL_IMG_URL).append(" = '")
                .append(customQ.getFullImgUrl().replace("'","''")).append("', ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.FULL_IMG_STORAGE_PATH).append(" = '")
                .append(customQ.getFullImgStoragePath().replace("'","''")).append("' where ")
                .append(Constants.DATABASE.CUSTOM_Q.COLUMNS.ID).append(" = ")
                .append(customQ.getId()).append("; ")
                .toString();
        db.execSQL(query);

    }

    //Alarm Table methods
    private void createAlarmTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ").append(Constants.DATABASE.ALARM.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.ALARM.COLUMNS.ID).append(" integer primary key autoincrement, ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TITLE).append(" text, ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TIME).append(" integer, ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TRACK_ID).append(" integer, ")
                .append(Constants.DATABASE.ALARM.COLUMNS.CUSTOM_Q_ID).append(" integer ")
                .append(")");
        db.execSQL(builder.toString());
    }

    private void dropAlarmTable(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("drop table ").append(Constants.DATABASE.ALARM.TABLE_NAME);
        db.execSQL(builder.toString());
    }

    public void saveAlarm(Alarm alarm) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("insert into ")
                .append(Constants.DATABASE.ALARM.TABLE_NAME)
                .append(" (")
                .append(Constants.DATABASE.ALARM.COLUMNS.TITLE).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TIME).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TRACK_ID).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.CUSTOM_Q_ID).append(") values ('")
                .append(alarm.getTitle()).append("', ")
                .append(alarm.getTime()).append(", ")
                .append(alarm.getTrackId()).append(", ")
                .append(alarm.getCustomQId()).append(") ")
                .toString();
        db.execSQL(query);

        query = new StringBuilder()
                .append("select max(")
                .append(Constants.DATABASE.ALARM.COLUMNS.ID).append(") from ")
                .append(Constants.DATABASE.ALARM.TABLE_NAME)
                .toString();
        Cursor result = db.rawQuery(query, null);
        if (result != null) {
            result.moveToFirst();
            alarm.setId(result.getLong(0));
        }

    }

    public void deleteAlarms(long timeFilter) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("delete from ")
                .append(Constants.DATABASE.ALARM.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TIME).append(" < ").append(timeFilter)
                .toString();
        db.execSQL(query);

    }

    public ArrayList<Alarm> loadAlarms(long timeFilter) {

        ArrayList<Alarm> alarms = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append("select ")
                .append(Constants.DATABASE.ALARM.COLUMNS.ID).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TITLE).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TIME).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TRACK_ID).append(", ")
                .append(Constants.DATABASE.ALARM.COLUMNS.CUSTOM_Q_ID).append(" from ")
                .append(Constants.DATABASE.ALARM.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TIME).append(" > ").append(timeFilter)
                .append(" order by ").append(Constants.DATABASE.ALARM.COLUMNS.TIME)
                .append(";")
                .toString();
        Cursor result = db.rawQuery(query, null);
        if (result == null) {
            return alarms;
        }
        while (result.moveToNext()) {
            Alarm alarm = new Alarm();
            alarm.setId(result.getLong(0));
            alarm.setTitle(result.getString(1));
            alarm.setTime(result.getLong(2));
            alarm.setTrackId(result.getLong(3));
            alarm.setCustomQId(result.getLong(4));
            alarms.add(alarm);
        }
        result.close();
        return alarms;

    }

    public void updateAlarm(Alarm alarm) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" update ")
                .append(Constants.DATABASE.ALARM.TABLE_NAME)
                .append(" set ")
                .append(Constants.DATABASE.ALARM.COLUMNS.TIME).append(" = ")
                .append(alarm.getTime()).append(" where ")
                .append(Constants.DATABASE.ALARM.COLUMNS.ID).append(" = ")
                .append(alarm.getId()).append(";")
                .toString();
        db.execSQL(query);

    }

    public void deleteAlarm(Alarm alarm) {

        SQLiteDatabase db = getWritableDatabase();
        String query = new StringBuilder()
                .append(" delete from ")
                .append(Constants.DATABASE.ALARM.TABLE_NAME)
                .append(" where ")
                .append(Constants.DATABASE.ALARM.COLUMNS.ID).append(" = ")
                .append(alarm.getId()).append(";")
                .toString();
        db.execSQL(query);

    }

}
