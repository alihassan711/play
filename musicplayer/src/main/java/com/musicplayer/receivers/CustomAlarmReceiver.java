package com.musicplayer.receivers;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.view.WindowManager;

import com.musicplayer.MusicPlayer;
import com.musicplayer.models.CustomQ;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;

public class CustomAlarmReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        long trackId = intent.getLongExtra("trackId", -1);
        long customQId = intent.getLongExtra("customQId", -1);
        long customQType = intent.getIntExtra("customQType", -1);
        long sleeperId = intent.getIntExtra("sleeperId", -1);
        if (trackId > -1) {

            if(!MusicPlayer.getSharedInstance().areTracksSetForPlay()) {
                MusicPlayer.getSharedInstance().setTracksForPlay();
            }
            Track track = MusicPlayer.getSharedInstance().getTracksHandler().getTracksWithId(trackId);
            if (track != null) {
                MusicPlayer.getSharedInstance().playTrack(track);
            }

        }else if (customQId > -1 && customQType > -1) {

            CustomQ customQ = null;
            if (customQType == Constants.DATABASE.CUSTOM_Q_TYPES.get("Playlist")){
                customQ = MusicPlayer.getSharedInstance().getPlaylistsHandler().getPlaylistWithId(customQId);
            }else if (customQType == Constants.DATABASE.CUSTOM_Q_TYPES.get("Album")){
                customQ = MusicPlayer.getSharedInstance().getAlbumsHandler().getAlbumWithId(customQId);
            }else if (customQType == Constants.DATABASE.CUSTOM_Q_TYPES.get("Artist")){
                customQ = MusicPlayer.getSharedInstance().getArtistsHandler().getArtistWithId(customQId);
            }

            if (customQ != null && customQ.getTracksList().size() > 0) {
                if (!MusicPlayer.getSharedInstance().isTracksQSetForPlay(customQ)) {
                    MusicPlayer.getSharedInstance().setTracksQForPlay(customQ);
                }
                MusicPlayer.getSharedInstance().playTrack(customQ.getTracksList().get(0));
            }

        }else if (sleeperId == 101) {

            MusicPlayer.getSharedInstance().pause();
            //Sleep screen

        }

    }

}
