package com.musicplayer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import com.musicplayer.MusicPlayer;
import com.musicplayer.services.AudioService;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

/**
 * Created by umair on 12/20/16.
 */

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_MEDIA_BUTTON)) {
            KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
            if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                return;

            switch (keyEvent.getKeyCode()) {
                case KeyEvent.KEYCODE_HEADSETHOOK:
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    if(MusicPlayer.getSharedInstance().isPlaying()){
                        MusicPlayer.getSharedInstance().pause();
                    }else{
                        MusicPlayer.getSharedInstance().play();
                    }
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    break;
                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    break;
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    MusicPlayer.getSharedInstance().playNextTrack();
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    MusicPlayer.getSharedInstance().playPreviousTrack();
                    break;
            }

        }  else{

            if (intent.getAction().equals(Constants.NOTIFICATION.NOTIFY_PREVIOUS)) {
                MusicPlayer.getSharedInstance().playPreviousTrack();
                MusicPlayer.getSharedInstance().play();
            }else if (intent.getAction().equals(Constants.NOTIFICATION.NOTIFY_PAUSE)) {
                MusicPlayer.getSharedInstance().pause();
            }else if (intent.getAction().equals(Constants.NOTIFICATION.NOTIFY_PLAY)) {
                MusicPlayer.getSharedInstance().play();
            } else if (intent.getAction().equals(Constants.NOTIFICATION.NOTIFY_NEXT)) {
                MusicPlayer.getSharedInstance().playNextTrack();
                MusicPlayer.getSharedInstance().play();
            } else if (intent.getAction().equals(Constants.NOTIFICATION.NOTIFY_CLOSE)) {
                MusicPlayer.getSharedInstance().closeApp();
            } else if (intent.getAction().equals(Constants.NOTIFICATION.NOTIFY_OPEN_APP)) {
                MusicPlayer.getSharedInstance().onNotificationClick(context);
            }
        }
    }

    public String componentName() {
        return this.getClass().getName();
    }

}
