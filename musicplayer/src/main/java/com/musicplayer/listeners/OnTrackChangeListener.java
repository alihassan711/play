package com.musicplayer.listeners;

import com.musicplayer.models.Track;

/**
 * Created by umair on 11/7/16.
 */
public interface OnTrackChangeListener {
    void onTrackChange(Track track);
    void onTrackCurrentPositionChange(long currentPosition);
    void onPlayPauseStateChange();
}
