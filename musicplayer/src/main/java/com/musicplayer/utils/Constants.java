package com.musicplayer.utils;

import java.util.HashMap;

/**
 * Created by umair on 11/5/16.
 */
public class Constants {

    public static boolean LoggingEnabled = true;

    public static class KEYS {
        public static String DB_INITIALIZED = "DB_INITIALIZED";
        public static String LAST_PLAYING_TRACK_ID = "LAST_PLAYING_TRACK_ID";
        public static String LAST_PLAYING_Q_ID = "LAST_PLAYING_Q_ID";
        public static String LAST_PLAYING_Q_TYPE = "LAST_PLAYING_Q_TYPE";
    }

    public static class DATABASE {

        public static String DB_NAME = "musicplayer.sqlite";
        public static int DB_VERSION = 1;

        public static class TRACK {
            public static String TABLE_NAME = "track";

            public static class COLUMNS {
                public static String ID = "id";
                public static String DURATION = "duration";
                public static String YEAR = "year";

                public static String PLAY_COUNT = "play_count";
                public static String CREATED_AT = "created_at";
                public static String LAST_PLAYED = "last_played";

                public static String STORAGE_ID = "storage_id";
                public static String TITLE = "title";
                public static String STORAGE_PATH = "storage_path";

                public static String YOUTUBE_TITLE = "youtube_title";
                public static String YOUTUBE_THUMB_URL = "youtube_thumb_url";
                public static String YOUTUBE_VIDEO_URL = "youtube_video_url";

                public static String THUMB_IMG_URL = "thumb_img_url";
                public static String THUMB_IMG_STORAGE_PATH = "thumb_img_storage_path";

                public static String FULL_IMG_URL = "full_img_url";
                public static String FULL_IMG_STORAGE_PATH = "full_img_storage_path";

            }

        }

        public static class SEGMENTED_TRACK {
            public static String TABLE_NAME = "segmented_track";

            public static class COLUMNS {
                public static String TRACK_ID = "track_id";
                public static String START_TIME = "start_time";
                public static String END_TIME = "end_time";
            }

        }

        public static class CUSTOM_Q {
            public static String TABLE_NAME = "custom_q";

            public static class COLUMNS {
                public static String ID = "id";
                public static String PLAY_COUNT = "play_count";
                public static String CREATED_AT = "created_at";

                public static String TITLE = "title";
                public static String STORAGE_ID = "storage_id";
                public static String CUSTOM_Q_TYPE_ID = "custom_q_type_id";

                public static String THUMB_IMG_URL = "thumb_img_url";
                public static String THUMB_IMG_STORAGE_PATH = "thumb_img_storage_path";

                public static String FULL_IMG_URL = "full_img_url";
                public static String FULL_IMG_STORAGE_PATH = "full_img_storage_path";
            }

        }

        public static class CUSTOM_Q_TYPE {
            public static String TABLE_NAME = "custom_q_type";

            public static class COLUMNS {
                public static String ID = "id";
                public static String TITLE = "title";
            }

        }

        public static class CUSTOM_Q_TRACKS {
            public static String TABLE_NAME = "custom_q_tracks";

            public static class COLUMNS {
                public static String CUSTOM_Q_ID = "custom_q_id";
                public static String TRACK_ID = "track_id";
            }

        }

        public static HashMap<String, Integer> CUSTOM_Q_TYPES = new HashMap<>();
        static {
            Constants.DATABASE.CUSTOM_Q_TYPES.put("MyQ", 1);
            Constants.DATABASE.CUSTOM_Q_TYPES.put("Artist", 2);
            Constants.DATABASE.CUSTOM_Q_TYPES.put("Genre", 3);
            Constants.DATABASE.CUSTOM_Q_TYPES.put("Album", 4);
            Constants.DATABASE.CUSTOM_Q_TYPES.put("Mood", 5);
            Constants.DATABASE.CUSTOM_Q_TYPES.put("Playlist", 6);
        }

        public static class ALARM {
            public static String TABLE_NAME = "alarm";

            public static class COLUMNS {
                public static String ID = "id";
                public static String TITLE = "title";
                public static String TIME = "time";
                public static String TRACK_ID = "track_id";
                public static String CUSTOM_Q_ID = "custom_q_id";
            }

        }

    }

    public static class SORT_ORDER {
        public static String A_TO_Z = "A_TO_Z";
        public static String Z_TO_A = "Z_TO_A";
        public static String CREATED_AT = "CREATED_AT";
        public static String YEAR = "YEAR";
        public static String PLAY_COUNT = "PLAY_COUNT";
    }

    public static class PREFERENCES {
        public static class KEYS {
            public static String TRACKS_SORT_ORDER = "TRACKS_SORT_ORDER";
            public static String PLAYLISTS_SORT_ORDER = "PLAYLISTS_SORT_ORDER";
            public static String ALBUMS_SORT_ORDER = "ALBUMS_SORT_ORDER";
            public static String ARTISTS_SORT_ORDER = "ARTISTS_SORT_ORDER";
        }

        public static class DEFAULTS {
            public static String TRACKS_SORT_ORDER = "A_TO_Z";
            public static String PLAYLISTS_SORT_ORDER = "A_TO_Z";
            public static String ALBUMS_SORT_ORDER = "A_TO_Z";
            public static String ARTISTS_SORT_ORDER = "A_TO_Z";
        }

    }

    public static class AUDIO_SETTINGS {

        public static class EQUALIZER_BANDS {
            public static class KEYS {
                public static String HZ_60 = "HZ_60";
                public static String HZ_100 = "HZ_100";
                public static String HZ_240 = "HZ_240";

                public static String HZ_500 = "HZ_500";
                public static String HZ_1000 = "HZ_1000";
                public static String HZ_1800 = "HZ_1800";

                public static String HZ_3400 = "HZ_3400";
                public static String HZ_5700 = "HZ_5700";
                public static String HZ_9000 = "HZ_9000";

                public static String HZ_13000 = "HZ_13000";
            }
            public static class DEFAULTS {
                public static int HZ_60 = 16;
                public static int HZ_100 = 16;
                public static int HZ_240 = 16;

                public static int HZ_500 = 16;
                public static int HZ_1000 = 16;
                public static int HZ_1800 = 16;

                public static int HZ_3400 = 16;
                public static int HZ_5700 = 16;
                public static int HZ_9000 = 16;

                public static int HZ_13000 = 16;
            }
            public static class FREQUENCIES {
                public static int HZ_60 = 60000;
                public static int HZ_100 = 100000;
                public static int HZ_240 = 240000;

                public static int HZ_500 = 500000;
                public static int HZ_1000 = 1000000;
                public static int HZ_1800 = 1800000;

                public static int HZ_3400 = 3400000;
                public static int HZ_5700 = 5700000;
                public static int HZ_9000 = 9000000;

                public static int HZ_13000 = 13000000;
            }
        }

        public static class KEYS {
            public static String BASSBOOST_STRENGTH = "BASSBOOST_STRENGTH";
            public static String VIRTUALIZER_STRENGTH = "VIRTUALIZER_STRENGTH";
            public static String PRESET_OPTION = "PRESET_OPTION";
            public static String TREBLE_STRENGTH = "TREBLE_STRENGTH";
        }
        public static class DEFAULTS {
            public static int BASSBOOST_STRENGTH = 0;
            public static int VIRTUALIZER_STRENGTH = 0;
            public static int PRESET_OPTION = 0;
            public static int TREBLE_STRENGTH = 500;
        }

    }

    public static String UNKNOWN_ARTIST = "Unknown Artist";
    public static String UNKNOWN_ALBUM = "Unknown Album";

    public static class NOTIFICATION {

        public static int ID = 982341;
        public static String NOTIFY_PREVIOUS = "com.joymix.musicplayer.previous";
        public static String NOTIFY_PAUSE = "com.joymix.musicplayer.pause";
        public static String NOTIFY_PLAY = "com.joymix.musicplayer.play";
        public static String NOTIFY_NEXT = "com.joymix.musicplayer.next";
        public static String NOTIFY_CLOSE = "com.joymix.musicplayer.close";
        public static String NOTIFY_OPEN_APP = "com.joymix.musicplayer.openapp";

    }


}
