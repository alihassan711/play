package com.musicplayer;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;

import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.handlers.AlarmsHandler;
import com.musicplayer.handlers.AlbumsHandler;
import com.musicplayer.handlers.ArtistsHandler;
import com.musicplayer.handlers.AudioSettingsHandler;
import com.musicplayer.handlers.GenresHandler;
import com.musicplayer.handlers.MoodsHandler;
import com.musicplayer.handlers.PlaylistsHandler;
import com.musicplayer.handlers.TracksHandler;
import com.musicplayer.listeners.OnTrackChangeListener;
import com.musicplayer.models.Album;
import com.musicplayer.models.Artist;
import com.musicplayer.models.CustomQ;
import com.musicplayer.models.Genre;
import com.musicplayer.models.Mood;
import com.musicplayer.models.MyQ;
import com.musicplayer.models.Playlist;
import com.musicplayer.models.SegmentedTrack;
import com.musicplayer.models.Track;
import com.musicplayer.services.AudioService;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by umair on 11/5/16.
 */
public class MusicPlayer {

    public static String TAG = "Music Player";

    public interface LoggingHandler {
        void addLogTime(String message, long time);
        void reportTime();
    }

    public interface OnNotificationClickListener {
        public void onNotificationClick(Context context);
    }

    private static MusicPlayer sharedInstance = new MusicPlayer();
    public static MusicPlayer getSharedInstance() {
        return sharedInstance;
    }

    private Context context;

    private TracksHandler tracksHandler = new TracksHandler();
    private MoodsHandler moodsHandler = new MoodsHandler();
    private ArtistsHandler artistsHandler = new ArtistsHandler();
    private GenresHandler genresHandler = new GenresHandler();
    private AlbumsHandler albumsHandler = new AlbumsHandler();
    private PlaylistsHandler playlistsHandler = new PlaylistsHandler();
    private MyQ myQ = new MyQ();

    private CustomQ currentQ;
    private int currentTrackIndex = 0;
    private Boolean isShuffleOn = false;
    private int repeat = 0; //Possible values 0 (No repeat), 1(Repeat Song), 2(Repeat All)
    private OnTrackChangeListener onTrackChangeListener;
    private Handler mediaHandler;
    private Boolean isPlaying = false, playCountIncrementPending = false;

    private AlarmsHandler alarmsHandler = new AlarmsHandler();
    private AudioSettingsHandler audioSettingsHandler;
    private Boolean shouldFilter = false, dataLoaded = false;
    private Boolean crossFade = false, fadeOutIn = false;
    private long fadeDelta = 30000;
    private OnNotificationClickListener onNotificationClickListener;
    private LoggingHandler loggingHandler = null;

    public Context getContext() {
        return context;
    }

    public Boolean prepareMusicPlayer(Context context, Boolean shouldFilter) {
        this.context = context;
        this.shouldFilter = shouldFilter;
        initialize();
        Boolean isFirstTime = Utilities.getValueForKey(context, Constants.KEYS.DB_INITIALIZED) == null;
        return isFirstTime;
    }

    private void initialize() {

        if (Utilities.getValueForKey(context, Constants.KEYS.DB_INITIALIZED) == null) {

            if (loggingHandler != null) {
                loggingHandler.addLogTime("New init start", System.currentTimeMillis());
            }
            loadBasicDataFromStorage();

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    loadFromStorage();
                    if (loggingHandler != null) {
                        loggingHandler.addLogTime("loaded all from storage", System.currentTimeMillis());
                    }
                    saveToDatabase();
                    if (loggingHandler != null) {
                        loggingHandler.addLogTime("Saved to db", System.currentTimeMillis());
                    }
                    refreshFilteredTracks(shouldFilter);
                    applyPreferences();
                    Utilities.saveValueForKey(context, "true", Constants.KEYS.DB_INITIALIZED);
                    dataLoaded = true;
                    if (loggingHandler != null) {
                        loggingHandler.addLogTime("new init compelte", System.currentTimeMillis());
                    }
                }
            });
            refreshFilteredTracks(shouldFilter);
            applyPreferences();
            initTrackQ();

        }else {

            if (loggingHandler != null) {
                loggingHandler.addLogTime("old init start", System.currentTimeMillis());
            }
            loadFromDatabase();
            if (loggingHandler != null) {
                loggingHandler.addLogTime("old init end", System.currentTimeMillis());
            }
            long t1 = System.currentTimeMillis();
            refreshFilteredTracks(shouldFilter);
            Log.e("joymix", "refreshFilteredTracks : " + (System.currentTimeMillis() - t1));
            t1 = System.currentTimeMillis();
            applyPreferences();
            Log.e("joymix", "applyPreferences : " + (System.currentTimeMillis() - t1));
            t1 = System.currentTimeMillis();
            initTrackQ();
            onTrackChange(getCurrentTrack());
            Log.e("joymix", "initTrackQ : " + (System.currentTimeMillis() - t1));
            dataLoaded = true;

        }

    }

    private void initTrackQ() {

        int trackId = Utilities.getIntValueForKey(context, Constants.KEYS.LAST_PLAYING_TRACK_ID, -1);
        if (trackId != -1) {

            currentQ = tracksHandler.getTracksQ();
            Track track = tracksHandler.getTracksWithId(trackId);
            if (track != null) {
                currentTrackIndex = findTrackIndex(currentQ, track);
            }else {
                currentTrackIndex = 0;
            }

        }else {
            currentQ = tracksHandler.getTracksQ();
        }
        playTrackHelper();

    }

    //Methods for loading data

    private void loadBasicDataFromStorage() {

        myQ.setId(1);
        moodsHandler.loadDefaults();
        playlistsHandler.loadDefaults();

        HashMap<String, HashMap<String, String>> rawTracks = new HashMap<>();
        loadRawTracksFromStorageHelper(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, rawTracks, 1);
        parseRawTracksToObjects(rawTracks);

    }

    private void loadFromStorage() {

        long t1 = System.currentTimeMillis();
        HashMap<String, HashMap<String, String>> rawTracks = new HashMap<>();
        loadRawTracksFromStorageHelper(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, rawTracks, -1);

        long t2 = System.currentTimeMillis();
        Log.e("joymix", "loadRawTracksFromStorageHelper: " + (t2 - t1));
        t1 = t2;

        parseRawTracksToObjects(rawTracks);
        Log.e("joymix", "parseRawTracksToObjects: " + (System.currentTimeMillis() - t1));

    }

    private void loadRawTracksFromStorageHelper(Uri storageUri, HashMap<String, HashMap<String, String>> rawTracks, int limit) {

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] mediaProjection = {
                MediaStore.Audio.Media._ID,//0
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DISPLAY_NAME,

                MediaStore.Audio.Media.ARTIST,//3
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.ALBUM,

                MediaStore.Audio.Media.DURATION,//6
                MediaStore.Audio.Media.DATE_ADDED,
                MediaStore.Audio.Media.YEAR,

                MediaStore.Audio.Media.DATA,//9

        };

        String orderLimit = MediaStore.Audio.Media.TITLE + " asc ";
        if (limit > 0) {
            orderLimit += " limit " + limit;
        }
        Cursor mediaCursor = context.getContentResolver().query(storageUri, mediaProjection, selection, null, orderLimit);
        if (mediaCursor == null) {
            return;
        }
        mediaCursor.moveToNext();//Skipping first
        while(mediaCursor.moveToNext()) {

            HashMap<String, String> rawTrack = new HashMap<>();
            rawTrack.put("storageId", mediaCursor.getInt(0)+"");
            rawTrack.put("title", mediaCursor.getString(1));
            rawTrack.put("displayName", mediaCursor.getString(2));
            String tmp = mediaCursor.getString(3);
            if (MediaStore.UNKNOWN_STRING.equals(tmp)) {
                tmp = Constants.UNKNOWN_ARTIST;
            }
            rawTrack.put("artist", tmp);
            rawTrack.put("albumId", mediaCursor.getLong(4)+"");
            tmp = mediaCursor.getString(5);
            if (MediaStore.UNKNOWN_STRING.equals(tmp)) {
                tmp = Constants.UNKNOWN_ALBUM;
            }
            rawTrack.put("album", tmp);
            rawTrack.put("duration", mediaCursor.getLong(6)+"");
            rawTrack.put("createdAt", mediaCursor.getLong(7)+"");
            rawTrack.put("year", mediaCursor.getLong(8)+"");
            rawTrack.put("storagePath", mediaCursor.getString(9));

            rawTracks.put(rawTrack.get("title").toLowerCase(), rawTrack);

        }
        mediaCursor.close();

    }

    private void parseRawTracksToObjects(HashMap<String, HashMap<String, String>> rawTracks) {

        String[] genresProjection = {
                MediaStore.Audio.Genres.NAME
        };

        Genre allGenre;
        if (genresHandler.getGenres().size() > 0) {
            allGenre = genresHandler.getGenres().get(0);
        }else {
            allGenre = new Genre(getNextCustomQId(), "All Genres");
            genresHandler.addGenre(allGenre);
        }


        int nextTrackId = DatabaseHandler.getSharedInstance(context).getNextTrackId();
        nextTrackId = nextTrackId >= tracksHandler.getTracks().size() + 1 ? nextTrackId : tracksHandler.getTracks().size() + 1;
        for (String key : rawTracks.keySet()) {

            HashMap<String, String> rawTrack = rawTracks.get(key);
            Track track = new Track(
                    ++nextTrackId,
                    Long.parseLong(rawTrack.get("duration")),
                    Long.parseLong(rawTrack.get("year")),
                    0,
                    Long.parseLong(rawTrack.get("createdAt")),
                    0,
                    Long.parseLong(rawTrack.get("albumId")),
                    Integer.parseInt(rawTrack.get("storageId")),
                    rawTrack.get("title"),
                    rawTrack.get("storagePath"),
                    rawTrack.get("album"),
                    rawTrack.get("artist"));
            tracksHandler.addTrack(track);

            CustomQ artist = getCustomQ(artistsHandler.getArtists(), rawTrack.get("artist"));
            if (artist == null) {
                artist = new Artist(getNextCustomQId(), rawTrack.get("artist"));
                artistsHandler.addArtist((Artist) artist);
            }
            artist.addTrackWithoutDuplicateCheck(track);

            CustomQ album = getCustomQ(albumsHandler.getAlbums(), rawTrack.get("album"));
            if (album == null) {
                album = new Album(getNextCustomQId(), rawTrack.get("album"));
                album.setStorageId(Long.parseLong(rawTrack.get("albumId")));
                albumsHandler.addAlbum((Album) album);
            }
            album.addTrackWithoutDuplicateCheck(track);

            Uri genreUri = MediaStore.Audio.Genres.getContentUriForAudioId("external", track.getStorageId());
            Cursor genresCursor = context.getContentResolver().query(genreUri, genresProjection, null, null, null);
            if (genresCursor != null) {
                while (genresCursor.moveToNext()) {
                    CustomQ genre = getCustomQ(genresHandler.getGenres(), genresCursor.getString(0));
                    if (genre == null) {
                        genre = new Genre(getNextCustomQId(), genresCursor.getString(0));
                        genresHandler.addGenre((Genre) genre);
                    }
                    genre.addTrackWithoutDuplicateCheck(track);
                }
                genresCursor.close();
            }

        }

        for (int i = 1; i < genresHandler.getGenres().size(); i++) {
            Genre genre = genresHandler.getGenres().get(i);
            for (Track track : genre.getAllTracks()) {
                allGenre.addTrackWithoutDuplicateCheck(track);
            }
        }

    }

    private CustomQ getCustomQ(ArrayList<? extends CustomQ> customQs, String title) {
        for (CustomQ customQ : customQs) {
            if (customQ.getTitle().equalsIgnoreCase(title)) {
                return customQ;
            }
        }
        return null;
    }

    private long getNextCustomQId() {
        long reserved = 15;
        return  artistsHandler.getArtists().size() + genresHandler.getGenres().size() + albumsHandler.getAlbums().size() + moodsHandler.getMoods().size() + playlistsHandler.getPlaylists().size() + reserved + 1;
    }

    private void saveToDatabase() {

        DatabaseHandler databaseHandler = DatabaseHandler.getSharedInstance(context);
        for (Track track : tracksHandler.getTracks()) {
            databaseHandler.saveTrack(track);
        }

        databaseHandler.saveCustomQ(myQ, Constants.DATABASE.CUSTOM_Q_TYPES.get("MyQ"));
        for(CustomQ artist : artistsHandler.getArtists()) {
            databaseHandler.saveCustomQ(artist, Constants.DATABASE.CUSTOM_Q_TYPES.get("Artist"));
        }
        for(CustomQ genre : genresHandler.getGenres()) {
            databaseHandler.saveCustomQ(genre, Constants.DATABASE.CUSTOM_Q_TYPES.get("Genre"));
        }
        for(CustomQ album : albumsHandler.getAlbums()) {
            databaseHandler.saveCustomQ(album, Constants.DATABASE.CUSTOM_Q_TYPES.get("Album"));
        }
        for(CustomQ mood : moodsHandler.getMoods()) {
            databaseHandler.saveCustomQ(mood, Constants.DATABASE.CUSTOM_Q_TYPES.get("Mood"));
        }
        for(CustomQ playlist : playlistsHandler.getPlaylists()) {
            databaseHandler.saveCustomQ(playlist, Constants.DATABASE.CUSTOM_Q_TYPES.get("Playlist"));
        }

    }

    private void loadFromDatabase() {

        Log.e("joymix", "load databse started");
        long t1 = System.currentTimeMillis();
        long t3 = t1;
        DatabaseHandler databaseHandler = DatabaseHandler.getSharedInstance(context);
//        databaseHandler.loadCustomQTypes();

        long t2 = System.currentTimeMillis();
        Log.e("joymix", "custom Q : " + (t2 - t1));
        t1 = t2;

        ArrayList<Track> tracksList = databaseHandler.loadTracks();

        t2 = System.currentTimeMillis();
        Log.e("joymix", "tracks1 : " + (t2 - t1));
        t1 = t2;

        tracksHandler.getTracksQ().getAllTracks().clear();
        tracksHandler.getTracksQ().getTracksList().clear();
        for (Track track : tracksList) {
            tracksHandler.addTrackWithoutDuplicateCheck(track);
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "tracks2 : " + (t2 - t1));
        t1 = t2;

        ArrayList<CustomQ> customQs;
        //Not loading data for myq
//        customQs = databaseHandler.loadCustomQs(Constants.DATABASE.CUSTOM_Q_TYPES.get("MyQ"), tracksList);
//        if (customQs.size() > 0) {
//            myQ.loadData(customQs.get(0));
//        }

        customQs = databaseHandler.loadCustomQs(Constants.DATABASE.CUSTOM_Q_TYPES.get("Artist"), tracksList);
        for (CustomQ customQ : customQs) {
            Artist artist = new Artist();
            artist.loadData(customQ);
            artistsHandler.addArtist(artist);
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "artist : " + (t2 - t1));
        t1 = t2;

        customQs = databaseHandler.loadCustomQs(Constants.DATABASE.CUSTOM_Q_TYPES.get("Genre"), tracksList);
        for (CustomQ customQ : customQs) {
            Genre genre = new Genre();
            genre.loadData(customQ);
            genresHandler.addGenre(genre);
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "genre : " + (t2 - t1));
        t1 = t2;

        customQs = databaseHandler.loadCustomQs(Constants.DATABASE.CUSTOM_Q_TYPES.get("Album"), tracksList);
        for (CustomQ customQ : customQs) {
            Album album = new Album();
            album.loadData(customQ);
            albumsHandler.addAlbum(album);
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "album : " + (t2 - t1));
        t1 = t2;

        customQs = databaseHandler.loadCustomQs(Constants.DATABASE.CUSTOM_Q_TYPES.get("Mood"), tracksList);
        for (CustomQ customQ : customQs) {
            Mood mood = new Mood();
            mood.loadData(customQ);
            moodsHandler.getMoods().add(mood);
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "mood : " + (t2 - t1));
        t1 = t2;

        customQs = databaseHandler.loadCustomQs(Constants.DATABASE.CUSTOM_Q_TYPES.get("Playlist"), tracksList);
        for (CustomQ customQ : customQs) {
            Playlist playlist = new Playlist();
            playlist.loadData(customQ);
            playlistsHandler.addPlaylist(playlist);
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "playlist : " + (t2 - t1));
        t1 = t2;

        Mood joymix = moodsHandler.getJoymix();
        if (joymix != null) {
            ArrayList<SegmentedTrack> segmentedTracks = databaseHandler.loadSegmentedTracks(tracksList);
            joymix.getAllTracks().clear();
            joymix.getTracksList().clear();
            for (SegmentedTrack segmentedTrack : segmentedTracks) {
                for(Album album : albumsHandler.getAlbums()) {
                    for(Track track : album.getAllTracks()) {
                        if (track.getId() == segmentedTrack.getId()) {
                            segmentedTrack.setAlbumId(album.getStorageId());
                            segmentedTrack.setAlbum(album.getTitle());
                        }
                    }
                }
                for(Artist artist : artistsHandler.getArtists()) {
                    for(Track track : artist.getAllTracks()) {
                        if (track.getId() == segmentedTrack.getId()) {
                            segmentedTrack.setArtist(artist.getTitle());
                        }
                    }
                }
                joymix.addTrackWithoutDuplicateCheck(segmentedTrack);
            }

        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "joymix : " + (t2 - t1));
        t1 = t2;

        for(Album album : albumsHandler.getAlbums()) {
            for(Track track : album.getAllTracks()) {
                track.setAlbumId(album.getStorageId());
                track.setAlbum(album.getTitle());
            }
        }

        for(Artist artist : artistsHandler.getArtists()) {
            for(Track track : artist.getAllTracks()) {
                track.setArtist(artist.getTitle());
            }
        }

        t2 = System.currentTimeMillis();
        Log.e("joymix", "ajust : " + (t2 - t1));
        t1 = t2;

        Log.e("joymix", "loadFromDatabase : " + (t1 - t3));

    }

    private void applyPreferences() {

        String tracksSortOrder = Utilities.getValueForKey(context, Constants.PREFERENCES.KEYS.TRACKS_SORT_ORDER);
        if (tracksSortOrder == null) {
            tracksSortOrder = Constants.PREFERENCES.DEFAULTS.TRACKS_SORT_ORDER;
        }
        tracksHandler.sortTracks(tracksSortOrder, null);

        String playlistsSortOrder = Utilities.getValueForKey(context, Constants.PREFERENCES.KEYS.PLAYLISTS_SORT_ORDER);
        if (playlistsSortOrder == null) {
            playlistsSortOrder = Constants.PREFERENCES.DEFAULTS.PLAYLISTS_SORT_ORDER;
        }
        playlistsHandler.sortPlaylists(playlistsSortOrder);
        genresHandler.sortGenres(Constants.SORT_ORDER.A_TO_Z);

        String albumsSortOrder = Utilities.getValueForKey(context, Constants.PREFERENCES.KEYS.ALBUMS_SORT_ORDER);
        if (albumsSortOrder == null) {
            albumsSortOrder = Constants.PREFERENCES.DEFAULTS.ALBUMS_SORT_ORDER;
        }
        albumsHandler.sortAlbums(albumsSortOrder);

        String artistsSortOrder = Utilities.getValueForKey(context, Constants.PREFERENCES.KEYS.ARTISTS_SORT_ORDER);
        if (artistsSortOrder == null) {
            artistsSortOrder = Constants.PREFERENCES.DEFAULTS.ARTISTS_SORT_ORDER;
        }
        artistsHandler.sortArtists(artistsSortOrder);

    }

    public HashMap<String, Boolean> checkTracksRefreshRequired() {

        HashMap<String, Boolean> refreshRequired = new HashMap<>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] mediaProjection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE
        };

        String orderLimit = MediaStore.Audio.Media.TITLE + " asc ";
        Cursor mediaCursor = context.getContentResolver().query(uri, mediaProjection, selection, null, orderLimit);
        if (mediaCursor == null) {
            refreshRequired.put("additionRequired", false);
            refreshRequired.put("deletionRequired", false);
            return refreshRequired;
        }

        HashMap<Integer, String> storageTracks = new HashMap<>();
        while(mediaCursor.moveToNext()) {
            storageTracks.put(mediaCursor.getInt(0), mediaCursor.getString(1));
        }
        mediaCursor.close();

        Boolean additionRequired = tracksHandler.checkTracksAdditionRequired(storageTracks);
        Boolean deletionRequired = tracksHandler.checkTracksDeletionRequired(storageTracks);

        refreshRequired.put("additionRequired", additionRequired);
        refreshRequired.put("deletionRequired", deletionRequired);
        return refreshRequired;

    }

    public void refreshTracks(HashMap<String, Boolean> refreshRequired) {

        HashMap<String, HashMap<String, String>> rawTracks = new HashMap<>();
        loadRawTracksFromStorageHelper(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, rawTracks, -1);

        if (refreshRequired.get("additionRequired")) {

            ArrayList<Track> newTracks = new ArrayList<>();
            DatabaseHandler databaseHandler = DatabaseHandler.getSharedInstance(context);

            int nextTrackId = databaseHandler.getNextTrackId();
            for (String key : rawTracks.keySet()) {

                HashMap<String, String> rawTrack = rawTracks.get(key);

                Track track = tracksHandler.getTracksWithTitle(rawTrack.get("title"));

                if (track == null) {

                    track = new Track(
                            ++nextTrackId,
                            Long.parseLong(rawTrack.get("duration")),
                            Long.parseLong(rawTrack.get("year")),
                            0,
                            Long.parseLong(rawTrack.get("createdAt")),
                            0,
                            Long.parseLong(rawTrack.get("albumId")),
                            Integer.parseInt(rawTrack.get("storageId")),
                            rawTrack.get("title"),
                            rawTrack.get("storagePath"),
                            rawTrack.get("album"),
                            rawTrack.get("artist"));

                    newTracks.add(track);
                    tracksHandler.addTrack(track);

                }

            }
            saveNewTracksToDatabase(newTracks);
            if (loggingHandler != null) {
                loggingHandler.addLogTime("new tracks added " + newTracks.size(), System.currentTimeMillis());
            }

        }
        if (refreshRequired.get("deletionRequired")) {

            ArrayList<Track> deletedTracks = new ArrayList<>();
            Boolean found;
            for (Track track : tracksHandler.getTracks()) {
                found = false;
                for (String key : rawTracks.keySet()) {
                    HashMap<String, String> rawTrack = rawTracks.get(key);
                    if (Integer.parseInt(rawTrack.get("storageId")) == track.getStorageId() && rawTrack.get("title").equalsIgnoreCase(track.getTitle())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    deletedTracks.add(track);
                }
            }

            for (Track track : deletedTracks) {
                tracksHandler.deleteTrack(track, false);
            }
            if (loggingHandler != null) {
                loggingHandler.addLogTime("Old tracks deleted " + deletedTracks.size(), System.currentTimeMillis());
            }

        }

    }

    public void saveNewTracksToDatabase(final ArrayList<Track> newTracks) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
            saveNewTracksToDatabaseHelper(newTracks);
            }
        });


    }

    private void saveNewTracksToDatabaseHelper(ArrayList<Track> newTracks) {
        String[] genresProjection = {
                MediaStore.Audio.Genres.NAME
        };

        DatabaseHandler databaseHandler = DatabaseHandler.getSharedInstance(context);
        int nextCustomQId = databaseHandler.getNextCustomQId();
        for (Track track : newTracks) {
            databaseHandler.saveTrack(track);

            CustomQ artist = getCustomQ(artistsHandler.getArtists(), track.getArtist());
            if (artist == null) {
                artist = new Artist(nextCustomQId++, track.getArtist());
                artistsHandler.addArtist((Artist) artist);
                databaseHandler.saveCustomQ(artist, Constants.DATABASE.CUSTOM_Q_TYPES.get("Artist"));
            }
            artist.addTrack(track);
            databaseHandler.addTrack(artist, track);

            CustomQ album = getCustomQ(albumsHandler.getAlbums(), track.getAlbum());
            if (album == null) {
                album = new Album(nextCustomQId++, track.getAlbum());
                album.setStorageId(track.getAlbumId());
                albumsHandler.addAlbum((Album) album);
                databaseHandler.saveCustomQ(album, Constants.DATABASE.CUSTOM_Q_TYPES.get("Album"));
            }
            album.addTrack(track);
            databaseHandler.addTrack(album, track);

            Uri genreUri = MediaStore.Audio.Genres.getContentUriForAudioId("external", track.getStorageId());
            Cursor genresCursor = context.getContentResolver().query(genreUri, genresProjection, null, null, null);
            if (genresCursor != null) {
                while (genresCursor.moveToNext()) {
                    CustomQ genre = getCustomQ(genresHandler.getGenres(), genresCursor.getString(0));
                    if (genre == null) {
                        genre = new Genre(nextCustomQId++, genresCursor.getString(0));
                        genresHandler.addGenre((Genre) genre);
                        databaseHandler.saveCustomQ(genre, Constants.DATABASE.CUSTOM_Q_TYPES.get("Genre"));
                    }
                    genre.addTrack(track);
                    databaseHandler.addTrack(genre, track);
                }
                genresCursor.close();
                if (genresHandler.getGenres().size() > 0) {
                    Genre allGenres = genresHandler.getGenres().get(0);
                    allGenres.addTrack(track);
                    databaseHandler.addTrack(allGenres, track);
                }
            }

        }
        applyPreferences();

    }

    public Boolean isDataLoaded() {
        return dataLoaded;
    }


    //Methods for others to access and process
    public void setLoggingHandler(LoggingHandler loggingHandler) {
        this.loggingHandler = loggingHandler;
    }

    public TracksHandler getTracksHandler() {
        return tracksHandler;
    }

    public MoodsHandler getMoodsHandler() {
        return moodsHandler;
    }

    public ArtistsHandler getArtistsHandler() {
        return artistsHandler;
    }

    public GenresHandler getGenresHandler() {
        return genresHandler;
    }

    public AlbumsHandler getAlbumsHandler() {
        return albumsHandler;
    }

    public PlaylistsHandler getPlaylistsHandler() {
        return playlistsHandler;
    }

    public MyQ getMyQ() {
        return myQ;
    }

    public Bitmap getAlbumArtForTrack(Track track, Boolean useIconDefault) {
        return getAlbumArtWithStorageId(track.getAlbumId(), useIconDefault);
    }

    public Bitmap getAlbumArtForTrack(Track track) {
        Bitmap albumArt = null;
        if (track.getFullImgStoragePath() != null &&  !track.getFullImgStoragePath().isEmpty()) {
            albumArt = Utilities.getLocalFileBitmap(track.getFullImgStoragePath());
        }
        if (albumArt == null){
            albumArt = getAlbumArtWithStorageId(track.getAlbumId(), false);
        }
        return albumArt;
    }

    public Bitmap getAlbumArtForAlbum(Album album) {
        return getAlbumArtWithStorageId(album.getStorageId(), true);
    }

    private Bitmap getAlbumArtWithStorageId(long storageId, Boolean useIconDefault) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try{
            final Uri artworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(artworkUri, storageId);
            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null){
                FileDescriptor fd = pfd.getFileDescriptor();
                Bitmap full = BitmapFactory.decodeFileDescriptor(fd, null, options);
                if (useIconDefault) {
                    int dimen = (int) Utilities.convertDpToPixel(100, context);
                    bm = Bitmap.createScaledBitmap(full, dimen, dimen, true);
                    if (bm != full) {
                        full.recycle();
                    }
                }else {
                    bm = full;
                }
                pfd = null;
                fd = null;
            }
        }
        catch(Error ee){}
        catch (Exception e) {}
        if (bm == null) {
            if (useIconDefault) {
                bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.album_icon_default);
            }else {
                bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.album_default);
            }
        }
        return bm;
    }

    public Bitmap getAlbumArtForTrackWithOffset(int offset) {
        if (currentQ == null || currentQ.getTracksList().size() == 0) {
            return null;
        }
        int index = currentTrackIndex + offset;
        if (index < 0) {
            index = currentQ.getTracksList().size() - 1;
        }else if (index >= currentQ.getTracksList().size()) {
            index = 0;
        }
        Track track = currentQ.getTracksList().get(index);
        return getAlbumArtForTrack(track);
    }

    public Uri getAlbumArtUri(long storageId) {
        final Uri artworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(artworkUri, storageId);
        return uri;
    }

    public AlarmsHandler getAlarmsHandler() {
        return alarmsHandler;
    }

    public void createAudioService(MediaPlayer mediaPlayer1, MediaPlayer mediaPlayer2) {
        if (context != null) {
            audioSettingsHandler = new AudioSettingsHandler(context, mediaPlayer1.getAudioSessionId(), mediaPlayer2.getAudioSessionId());
        }
    }

    public AudioSettingsHandler getAudioSettingsHandler() {
        return audioSettingsHandler;
    }

    //Methods for playing
    public void setOnTrackChangeListener(OnTrackChangeListener onTrackChangeListener) {
        this.onTrackChangeListener = onTrackChangeListener;
    }

    public void setMediaHandler(Handler mediaHandler) {
        this.mediaHandler = mediaHandler;
    }

    public Boolean isPlaying() {
        return isPlaying;
    }

    public Boolean isShuffleOn() {
        return isShuffleOn;
    }

    public void setShuffleOn(Boolean suffleOn) {
        isShuffleOn = suffleOn;
    }

    public int getRepeat() {
        return repeat;
    }

    public void toggleRepeat() {
        repeat += 1;
        repeat %= 3;
    }

    public void setTracksQForPlay(CustomQ tracksQ) {
        currentQ = tracksQ;
        currentTrackIndex = 0;
        tracksQ.setPlayCount(tracksQ.getPlayCount()+1);
        DatabaseHandler.getSharedInstance(context).updatePlayCount(tracksQ);
    }

    public Boolean isTracksQSetForPlay(CustomQ tracksQ) {
        return currentQ == tracksQ;
    }

    public void setTracksForPlay() {
        currentQ = tracksHandler.getTracksQ();
        currentTrackIndex = 0;
    }

    public Boolean areTracksSetForPlay() {
        return currentQ == tracksHandler.getTracksQ();
    }

    public void playTrack(Track track) {

        currentTrackIndex = findTrackIndex(currentQ, track);
        isPlaying = true;
        playTrackHelper();

    }

    private void playTrackHelper() {
        if (!Utilities.isServiceRunning(context, AudioService.class.getName())) {
            Intent i = new Intent(context, AudioService.class);
            context.startService(i);
        }else if (mediaHandler != null){
            mediaHandler.sendMessage(mediaHandler.obtainMessage(0, "prepare"));
        }
        if (isPlaying) {
            updatePlayCountAndTime(getCurrentTrack());
        }else {
            playCountIncrementPending = true;
        }
    }

    public void play() {
        isPlaying = true;
        if (!Utilities.isServiceRunning(context, AudioService.class.getName())) {
            Intent i = new Intent(context, AudioService.class);
            context.startService(i);
        }else if (mediaHandler != null) {
            mediaHandler.sendMessage(mediaHandler.obtainMessage(0, "play"));
        }
        updatePlayCountAndTime(getCurrentTrack());
    }

    public void pause() {
        if (!isPlaying()) {
            return;
        }
        isPlaying = false;
        if (mediaHandler != null) {
            mediaHandler.sendMessage(mediaHandler.obtainMessage(0, "pause"));
        }
    }

    public void stop() {
        Intent i = new Intent(context, AudioService.class);
        context.stopService(i);
    }

    public void playPreviousTrack() {
        if (isShuffleOn()) {
            playRandomTrack();
        }else {
            currentTrackIndex -= 1;
            if (currentTrackIndex < 0) {
                currentTrackIndex = currentQ.getTracksList().size() - 1;
            }
            playTrackHelper();
        }
    }

    public void playNextTrack() {
        if (isShuffleOn()) {
            playRandomTrack();
        }else {
            currentTrackIndex += 1;
            if (currentTrackIndex >= currentQ.getTracksList().size()) {
                currentTrackIndex = 0;
            }
            playTrackHelper();
        }
    }

    public void playNextTrackWithoutPrepare() {
        currentTrackIndex += 1;
        if (currentTrackIndex >= currentQ.getTracksList().size()) {
            currentTrackIndex = 0;
        }
        if (isPlaying) {
            play();
        }else {
            playCountIncrementPending = true;
        }
    }

    public void onTrackChange(Track track) {
        if (onTrackChangeListener != null) {
            onTrackChangeListener.onTrackChange(track);
        }
    }

    public void onTrackCurrentPositionChange(long currentPosition) {
        if (onTrackChangeListener != null) {
            onTrackChangeListener.onTrackCurrentPositionChange(currentPosition);
        }
    }

    public void onPlayPauseStateChange() {
        if (onTrackChangeListener != null) {
            onTrackChangeListener.onPlayPauseStateChange();
        }
    }

    public void changeTrackCurrentPosition(long currentPosition) {
        mediaHandler.sendMessage(mediaHandler.obtainMessage(0, (int)currentPosition, 0, "changeCurrent"));
    }

    public Track getCurrentTrack() {
        if (currentQ != null && currentQ.getTracksList().size() > 0 && currentTrackIndex > -1 && currentTrackIndex < currentQ.getTracksList().size()) {
            return currentQ.getTracksList().get(currentTrackIndex);
        }
        return null;
    }

    public Track getNextTrack() {
        if (currentQ == null) {
            return null;
        }
        int nextTrackIndex = currentTrackIndex + 1;
        if (nextTrackIndex >= currentQ.getTracksList().size()) {
            nextTrackIndex = 0;
        }
        if (nextTrackIndex < currentQ.getTracksList().size()) {
            return currentQ.getTracksList().get(nextTrackIndex);
        }
        return null;
    }

    public int getCurrentTrackIndex() {
        return currentTrackIndex;
    }

    public Boolean isCurrentTrackLast() {
        return currentTrackIndex == currentQ.getTracksList().size() - 1;
    }

    public void playRandomTrack() {
        Random random = new Random();
        currentTrackIndex = random.nextInt(currentQ.getTracksList().size());
        playTrackHelper();
    }

    private int findTrackIndex(CustomQ customQ, Track track) {
        for(int i = 0; i < customQ.getTracksList().size(); i++) {
            if(customQ.getTracksList().get(i).getTitle().equals(track.getTitle())) {
                return i;
            }
        }
        return 0;
    }

    public void updatePlayCountAndTime(Track track) {
        if (track == null) {
            return;
        }
        track.setPlayCount(track.getPlayCount()+1);
        track.setLastPlayed(new Date().getTime());
        DatabaseHandler.getSharedInstance(context).updatePlayCountAndTime(track);
    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        tracksHandler.refreshFilteredTracks(shouldFilter);
        moodsHandler.refreshFilteredTracks(shouldFilter);
        artistsHandler.refreshFilteredTracks(shouldFilter);
        genresHandler.refreshFilteredTracks(shouldFilter);
        albumsHandler.refreshFilteredTracks(shouldFilter);
        playlistsHandler.refreshFilteredTracks(shouldFilter);
        myQ.refreshFilteredTracks(shouldFilter);
        applyPreferences();
    }


    public Boolean isCrossFadeOn() {
        return crossFade;
    }

    public void setCrossFade(Boolean crossFade) {
        this.crossFade = crossFade;
    }

    public Boolean isFadeOutInOn() {
        return fadeOutIn;
    }

    public void setFadeOutIn(Boolean fadeOutIn) {
        this.fadeOutIn = fadeOutIn;
    }

    public long getFadeDelta() {
        return fadeDelta;
    }

    public void setFadeDelta(long fadeDelta) {
        this.fadeDelta = fadeDelta;
    }

    public void release() {
        if (!isPlaying() && audioSettingsHandler != null) {
            audioSettingsHandler.releaseEqualizer();
        }
    }

    public void reallocate() {
        if (audioSettingsHandler != null) {
            audioSettingsHandler.initEqualizer();
        }
    }


    public void setOnNotificationClickListener(OnNotificationClickListener onNotificationClickListener) {
        this.onNotificationClickListener = onNotificationClickListener;
    }

    public void onNotificationClick(Context context) {
        if (onNotificationClickListener != null) {
            onNotificationClickListener.onNotificationClick(context);
        }
    }

    public void closeApp() {
        pause();
        if (Utilities.isServiceRunning(context, AudioService.class.getName())) {
            Intent i = new Intent(context, AudioService.class);
            context.stopService(i);
        }
        System.exit(0);
    }

}
