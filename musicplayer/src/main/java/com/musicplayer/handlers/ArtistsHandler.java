package com.musicplayer.handlers;

import android.content.Context;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Artist;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 11/13/16.
 */
public class ArtistsHandler extends BaseHandler {

    private ArrayList<Artist> artists = new ArrayList<>();

    public ArtistsHandler() {

    }

    public ArrayList<Artist> getArtists() {
        return artists;
    }

    public Boolean addArtist(Artist artist) {

        if (artists.contains(artist)) {
            return false;
        }

        for (Artist art : artists) {
            if (art.getTitle().equalsIgnoreCase(artist.getTitle())) {
                return  false;
            }
        }

        artists.add(artist);
        return true;

    }

    public void sortArtists(String sortOrder) {
        sortCustomQs(artists, sortOrder);
        Utilities.saveValueForKey(MusicPlayer.getSharedInstance().getContext(), sortOrder, Constants.PREFERENCES.KEYS.ARTISTS_SORT_ORDER);
    }

    private void sortWithCurrentOrder() {
        String albumsSortOrder = Utilities.getValueForKey(MusicPlayer.getSharedInstance().getContext(), Constants.PREFERENCES.KEYS.ARTISTS_SORT_ORDER);
        if (albumsSortOrder == null) {
            albumsSortOrder = Constants.PREFERENCES.DEFAULTS.ARTISTS_SORT_ORDER;
        }
        sortArtists(albumsSortOrder);
    }

    public void deleteArtist(Artist artist) {

        List<Track> tracks = artist.getAllTracks();
        for (int  i = 0; i < tracks.size();) {
            MusicPlayer.getSharedInstance().getTracksHandler().deleteTrack(tracks.get(i));
        }

        Context context = MusicPlayer.getSharedInstance().getContext();
        DatabaseHandler.getSharedInstance(context).deleteCustomQ(artist);
        artists.remove(artist);
        sortWithCurrentOrder();

    }

    public void changeArtistTitle(Artist artist, String newTitle) {
        artist.setTitle(newTitle);
        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).changeTitleCustomQ(artist);
        sortWithCurrentOrder();
    }

    public ArrayList<Artist> getArtistsWithIndices(ArrayList<Integer> indices) {
        ArrayList<Artist> result = new ArrayList<>();
        for (Integer index : indices) {
            if (index < artists.size()) {
                result.add(artists.get(index));
            }
        }
        return result;
    }

    public void deleteArtistsWithIndices(ArrayList<Integer> indices) {

        for (Integer index : indices) {
            if (index < artists.size()) {
                deleteArtist(artists.get(index));
            }
        }
        sortWithCurrentOrder();

    }

    public Artist getArtistContainingTrack(Track trackToSearch) {
        for (Artist artist : artists) {
            for (Track track : artist.getTracksList()) {
                if (track.getId() == trackToSearch.getId()) {
                    return artist;
                }
            }
        }
        return null;
    }

    public Artist getArtistWithId(long artistId) {
        for (Artist artist : artists) {
            if (artist.getId() == artistId) {
                return artist;
            }
        }
        return null;
    }

    public ArrayList<Artist> filterArtists(String text) {
        if (text.isEmpty()) {
            return artists;
        }
        ArrayList<Artist> tmpArtists = new ArrayList<>();
        for (Artist artist : artists) {
            if (artist.getTitle().toLowerCase().startsWith(text)) {
                tmpArtists.add(artist);
            }
        }
        return tmpArtists;
    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        for (Artist artist : artists) {
            artist.refreshFilteredTracks(shouldFilter);
        }
    }

}
