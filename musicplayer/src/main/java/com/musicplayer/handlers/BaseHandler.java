package com.musicplayer.handlers;

import com.musicplayer.models.CustomQ;
import com.musicplayer.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by umair on 11/13/16.
 */
public class BaseHandler {

    protected void sortCustomQs(ArrayList<? extends CustomQ> customQs, String sortOrder) {

        Comparator<CustomQ> comparator;
        if (sortOrder.equals(Constants.SORT_ORDER.A_TO_Z)) {
            comparator = new Comparator<CustomQ>() {
                @Override
                public int compare(CustomQ lhs, CustomQ rhs) {
                    return lhs.getTitle().toLowerCase().compareTo(rhs.getTitle().toLowerCase());
                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.Z_TO_A)) {
            comparator = new Comparator<CustomQ>() {
                @Override
                public int compare(CustomQ lhs, CustomQ rhs) {
                    return rhs.getTitle().toLowerCase().compareTo(lhs.getTitle().toLowerCase());
                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.CREATED_AT)) {
            comparator = new Comparator<CustomQ>() {
                @Override
                public int compare(CustomQ lhs, CustomQ rhs) {
                    return lhs.getCreatedAt() > rhs.getCreatedAt() ? 1 : lhs.getCreatedAt() < rhs.getCreatedAt() ? -1 : 0;
                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.PLAY_COUNT)) {
            comparator = new Comparator<CustomQ>() {
                @Override
                public int compare(CustomQ lhs, CustomQ rhs) {
                    return lhs.getPlayCount() > rhs.getPlayCount() ? 1 : lhs.getPlayCount() < rhs.getPlayCount() ? -1 : 0;
                }
            };
        }else {
            comparator = new Comparator<CustomQ>() {
                @Override
                public int compare(CustomQ lhs, CustomQ rhs) {
                    return lhs.getId() > rhs.getId() ? 1 : lhs.getId() < rhs.getId() ? -1 : 0;
                }
            };
        }
        Collections.sort(customQs, comparator);

    }

}
