package com.musicplayer.handlers;

import android.content.Context;
import android.provider.MediaStore;
import android.util.Log;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Album;
import com.musicplayer.models.Artist;
import com.musicplayer.models.CustomQ;
import com.musicplayer.models.Genre;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by umair on 11/13/16.
 */
public class TracksHandler {

    CustomQ tracks = new CustomQ();

    public TracksHandler() {
        tracks.setId(8888);
        tracks.setTitle("Tracks");
    }

    public CustomQ getTracksQ() {
        return tracks;
    }

    public ArrayList<Track> getTracks() {
        return tracks.getTracksList();
    }

    public void addTrack(Track track) {
        tracks.addTrack(track);
    }

    public void addTrackWithoutDuplicateCheck(Track track) {
        tracks.addTrackWithoutDuplicateCheck(track);
    }

    public void deleteTrack(Track track) {
        deleteTrack(track, true);
    }

    public void deleteTrack(Track track, Boolean deleteFromStorage) {

        if (deleteFromStorage) {
            Boolean deleted = false;
            try {
                File file = new File(track.getStoragePath());
                if (file.exists()) {
                    deleted = file.delete();
                    Log.e("joymix", "deleted: " + deleted);
                }
            } catch (Exception e) {
                Log.d("joymix", "file: '" + track.getStoragePath() + "' couldn't be deleted", e);
            }
            if (deleted) {
                MusicPlayer.getSharedInstance().getContext().getContentResolver().delete(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, MediaStore.Audio.Media._ID + "=" + track.getStorageId(), null);
            }

        }


        Context context = MusicPlayer.getSharedInstance().getContext();
        DatabaseHandler.getSharedInstance(context).deleteTrack(track);

        tracks.deleteTrack(track);
        MusicPlayer.getSharedInstance().getMyQ().deleteTrack(track);

        ArrayList<CustomQ> customQsToDeleted = new ArrayList<>();
        ArtistsHandler artistsHandler = MusicPlayer.getSharedInstance().getArtistsHandler();
        for(CustomQ customQ : artistsHandler.getArtists()) {
            if (customQ.containTrack(track)){
                customQ.deleteTrack(track);
                if (customQ.isTrackListEmpty()) {
                    customQsToDeleted.add(customQ);
                }
            }
        }

        for (CustomQ customQ : customQsToDeleted) {
            artistsHandler.deleteArtist((Artist) customQ);
        }

        customQsToDeleted.clear();
        GenresHandler genresHandler = MusicPlayer.getSharedInstance().getGenresHandler();
        for(CustomQ customQ : genresHandler.getGenres()) {
            if (customQ.containTrack(track)){
                customQ.deleteTrack(track);
                if (customQ.isTrackListEmpty()) {
                    customQsToDeleted.add(customQ);
                }
            }
        }

        for (CustomQ customQ : customQsToDeleted) {
            genresHandler.deleteGenre((Genre) customQ);
        }

        customQsToDeleted.clear();
        AlbumsHandler albumsHandler =  MusicPlayer.getSharedInstance().getAlbumsHandler();
        for(CustomQ customQ : albumsHandler.getAlbums()) {
            if (customQ.containTrack(track)){
                customQ.deleteTrack(track);
                if (customQ.isTrackListEmpty()) {
                    customQsToDeleted.add(customQ);
                }
            }
        }
        for (CustomQ customQ : customQsToDeleted) {
            albumsHandler.deleteAlbum((Album) customQ);
        }

        for(CustomQ customQ : MusicPlayer.getSharedInstance().getMoodsHandler().getMoods()) {
            if (customQ.containTrack(track)){
                customQ.deleteTrack(track);
            }
        }
        for(CustomQ customQ : MusicPlayer.getSharedInstance().getPlaylistsHandler().getPlaylists()) {
            if (customQ.containTrack(track)){
                customQ.deleteTrack(track);
            }
        }

    }

    public void sortTracks(String sortOrder, CustomQ.OnSortCompleteCallback onSortCompleteCallback) {
        tracks.sortTracks(sortOrder, onSortCompleteCallback);
        Utilities.saveValueForKey(MusicPlayer.getSharedInstance().getContext(), sortOrder, Constants.PREFERENCES.KEYS.TRACKS_SORT_ORDER);
    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        tracks.refreshFilteredTracks(shouldFilter);
    }

    public ArrayList<Track> getTopPlayed() {

        ArrayList<Track> result = new ArrayList<>(tracks.getTracksList());
        Collections.sort(result, new Comparator<Track>() {
            @Override
            public int compare(Track lhs, Track rhs) {
                long l1 = lhs.getPlayCount(), l2 = rhs.getPlayCount();
                return l1 < l2 ? 1: l1 > l2 ? -1 : 0;
            }
        });
        for(int i = 20; i < result.size(); ) {
            result.remove(result.get(i));
        }
        return result;

    }

    public ArrayList<Track> getRecentPlayed() {

        ArrayList<Track> result = new ArrayList<>(tracks.getTracksList());
        Collections.sort(result, new Comparator<Track>() {
            @Override
            public int compare(Track lhs, Track rhs) {
                long l1 = lhs.getLastPlayed(), l2 = rhs.getLastPlayed();
                return l1 < l2 ? 1: l1 > l2 ? -1 : 0;
            }
        });
        for(int i = 20; i < result.size(); ) {
            result.remove(result.get(i));
        }
        return result;

    }

    public ArrayList<Track> getRecentAdded() {

        ArrayList<Track> result = new ArrayList<>(tracks.getTracksList());
        Collections.sort(result, new Comparator<Track>() {
            @Override
            public int compare(Track lhs, Track rhs) {
                long l1 = lhs.getCreatedAt(), l2 = rhs.getCreatedAt();
                return l1 < l2 ? 1: l1 > l2 ? -1 : 0;
            }
        });
        for(int i = 20; i < result.size(); ) {
            result.remove(result.get(i));
        }
        return result;

    }

    public Track getTracksWithId(long trackId) {
        for (Track track : tracks.getTracksList()) {
            if (track.getId() == trackId) {
                return track;
            }
        }
        return null;
    }

//    public Track getTracksWithStorageIdTitle(long storageId, String title) {
//        for (Track track : tracks.getAllTracks()) {
//            if (track.getStorageId() == storageId && track.getTitle().equalsIgnoreCase(title)) {
//                return track;
//            }
//        }
//        return null;
//    }

    public Track getTracksWithTitle(String title) {
        for (Track track : tracks.getAllTracks()) {
            if (track.getTitle().equalsIgnoreCase(title)) {
                return track;
            }
        }
        return null;
    }

    public Boolean checkTracksAdditionRequired(HashMap<Integer, String> storageTracks) {
        Boolean found;
        for (Integer storageId : storageTracks.keySet()) {
            found = false;
            for (Track track : tracks.getAllTracks()) {
                if (track.getStorageId() == storageId && track.getTitle().equalsIgnoreCase(storageTracks.get(storageId))) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return true;
            }
        }
        return false;
    }

    public Boolean checkTracksDeletionRequired(HashMap<Integer, String> storageTracks) {
        for (Track track : tracks.getAllTracks()) {
            if (!storageTracks.containsKey(track.getStorageId())) {
                return true;
            }
        }
        return false;
    }

    public Track getTrackWithStoragePath(String storagePath) {
        for (Track track : tracks.getTracksList()) {
            if (track.getStoragePath().equalsIgnoreCase(storagePath)) {
                return track;
            }
        }
        return null;
    }

}
