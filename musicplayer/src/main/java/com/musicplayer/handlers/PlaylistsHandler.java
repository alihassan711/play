package com.musicplayer.handlers;

import android.content.Context;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Playlist;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.util.ArrayList;

/**
 * Created by umair on 11/13/16.
 */
public class PlaylistsHandler extends BaseHandler {

    private ArrayList<Playlist> playlists = new ArrayList<>();

    public PlaylistsHandler() {

    }

    public void loadDefaults() {
        playlists.add(new Playlist(10, "Top Played"));
        playlists.add(new Playlist(11, "Recently Played"));
        playlists.add(new Playlist(12, "Recently Added"));
    }

    public ArrayList<Playlist> getPlaylists() {
        return playlists;
    }

    public Boolean addPlaylist(Playlist playlist) {

        if (playlists.contains(playlist)) {
            return false;
        }

        for (Playlist ply : playlists) {
            if (ply.getTitle().equalsIgnoreCase(playlist.getTitle())) {
                return  false;
            }
        }

        playlists.add(playlist);
        return true;

    }

    public void addPlaylist(String title) {
        DatabaseHandler databaseHandler = DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext());
        Playlist playlist = new Playlist(databaseHandler.getNextCustomQId(), title);
        databaseHandler.saveCustomQ(playlist, Constants.DATABASE.CUSTOM_Q_TYPES.get("Playlist"));
        playlists.add(playlist);
        sortWithCurrentOrder();
    }


    private void sortWithCurrentOrder() {
        String playlistsSortOrder = Utilities.getValueForKey(MusicPlayer.getSharedInstance().getContext(), Constants.PREFERENCES.KEYS.PLAYLISTS_SORT_ORDER);
        if (playlistsSortOrder == null) {
            playlistsSortOrder = Constants.PREFERENCES.DEFAULTS.PLAYLISTS_SORT_ORDER;
        }
        sortPlaylists(playlistsSortOrder);
    }

    public void sortPlaylists(String sortOrder) {
        ArrayList<Playlist> customPlaylists = new ArrayList<>();
        for (int i = 3; i < playlists.size();) {
            Playlist playlist = playlists.get(i);
            customPlaylists.add(playlist);
            playlists.remove(playlist);
        }
        sortCustomQs(customPlaylists, sortOrder);
        for (Playlist playlist : customPlaylists) {
            playlists.add(playlist);
        }
        Utilities.saveValueForKey(MusicPlayer.getSharedInstance().getContext(), sortOrder, Constants.PREFERENCES.KEYS.PLAYLISTS_SORT_ORDER);
    }

    public Boolean customPlaylistsCreated() {
        return playlists.size() > 3;
    }


    public ArrayList<String> getPlaylistsTitles() {
        ArrayList<String> titles = new ArrayList<>();
        for (int i = 3; i < playlists.size(); i++) {
            Playlist playlist = playlists.get(i);
            titles.add(playlist.getTitle());
        }
        return titles;
    }

    public ArrayList<Playlist> getPlaylistsWithIndices(ArrayList<Integer> indices) {
        ArrayList<Playlist> result = new ArrayList<>();
        for (Integer index : indices) {
            if (index + 3 < playlists.size()) {
                result.add(playlists.get(index + 3));
            }
        }
        return result;
    }

    public void deletePlaylistsWithIndices(ArrayList<Integer> indices) {

        for (Integer index : indices) {
            if (index < playlists.size()) {
                deletePlaylist(playlists.get(index));
            }
        }
        sortWithCurrentOrder();

    }


    public void deletePlaylist(Playlist playlist) {

        Context context = MusicPlayer.getSharedInstance().getContext();
        DatabaseHandler.getSharedInstance(context).deleteCustomQ(playlist);
        playlists.remove(playlist);
        sortWithCurrentOrder();

    }

    public void changePlaylistTitle(Playlist playlist, String newTitle) {
        playlist.setTitle(newTitle);
        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).changeTitleCustomQ(playlist);
        sortWithCurrentOrder();
    }


    public Playlist getPlaylistContainingTrack(Track trackToSearch) {
        for (Playlist playlist : playlists) {
            for (Track track : playlist.getTracksList()) {
                if (track.getId() == trackToSearch.getId()) {
                    return playlist;
                }
            }
        }
        return null;
    }

    public Playlist getPlaylistWithTitle(String title) {
        title = title.toLowerCase();
        for (Playlist playlist : playlists) {
            if (playlist.getTitle().toLowerCase().equals(title)) {
                return playlist;
            }
        }
        return null;
    }

    public Playlist getPlaylistWithId(long playlistId) {
        for (Playlist playlist : playlists) {
            if (playlist.getId() == playlistId) {
                return playlist;
            }
        }
        return null;
    }

    public ArrayList<Playlist> filterPlaylists(String text) {

        ArrayList<Playlist> tmpPlaylists = new ArrayList<>();
        if (text.isEmpty()) {
            for (int i = 3; i < playlists.size(); i++) {
                Playlist playlist = playlists.get(i);
                tmpPlaylists.add(playlist);
            }
        }else {
            for (int i = 3; i < playlists.size(); i++) {
                Playlist playlist = playlists.get(i);
                if (playlist.getTitle().toLowerCase().startsWith(text)) {
                    tmpPlaylists.add(playlist);
                }
            }
        }
        return tmpPlaylists;
    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        for (Playlist playlist : playlists) {
            playlist.refreshFilteredTracks(shouldFilter);
        }
    }

}
