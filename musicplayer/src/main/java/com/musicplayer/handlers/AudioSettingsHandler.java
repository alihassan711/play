package com.musicplayer.handlers;

import android.content.Context;
import android.media.AudioManager;
import android.media.audiofx.AudioEffect;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.media.audiofx.Virtualizer;
import android.os.Build;
import android.util.Log;

import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.util.ArrayList;
import java.util.UUID;

import static android.content.Context.AUDIO_SERVICE;

/**
 * Created by muhammadbhatti on 01/12/2016.
 */
public class AudioSettingsHandler {

    private Context context;
    private Equalizer equalizer1;
    private Equalizer equalizer2;
    private Virtualizer virtualizer1;
    private Virtualizer virtualizer2;
    private BassBoost bassBoost1;
    private BassBoost bassBoost2;

    private int hz60Level = 16;
    private int hz100Level = 16;
    private int hz240Level = 16;

    private int hz500Level = 16;
    private int hz1000Level = 16;
    private int hz1800Level = 16;

    private int hz3400Level = 16;
    private int hz5700Level = 16;
    private int hz9000Level = 16;

    private int hz13000Level = 16;

    private int bassBoostStrength = 0;
    private int virtualizerStrength = 0;
    private int presetOption = 0;
    private int trebleStrength = 16;

    private AudioManager audioManager;
    private ArrayList<String> presetOptions;
    private int maxVolume;
    private int audioSessionId1  = -1, audioSessionId2 = -1;

    private boolean isEqualizerAvailable = false;


    public AudioSettingsHandler(Context context, int audioSessionId1, int audioSessionId2) {

        this.context = context;
        this.audioSessionId1 = audioSessionId1;
        this.audioSessionId2 = audioSessionId2;

        isEqualizerAvailable = equalizerAvailable();

        presetOptions = new ArrayList<>();
        presetOptions.add("None");

        presetOptions.add("Flat");
        presetOptions.add("Rock");
        presetOptions.add("Hip Hop");

        presetOptions.add("Jazz");
        presetOptions.add("Classical");
        presetOptions.add("Heavy Metal");

        presetOptions.add("Folk");
        presetOptions.add("Dance");
        presetOptions.add("Soft");

        audioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        initEqualizer();

    }

    private static final UUID EQUALIZER_UUID;

    static {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            EQUALIZER_UUID = AudioEffect.EFFECT_TYPE_EQUALIZER;
        } else {
            EQUALIZER_UUID = UUID.fromString("0bed4300-ddd6-11db-8f34-0002a5d5c51b");
        }
    }


    private boolean equalizerAvailable() {
        for (AudioEffect.Descriptor effect : AudioEffect.queryEffects()) {
            if (EQUALIZER_UUID.equals(effect.type)) {
                return true;
            }
        }
        return false;
    }

    public void initEqualizer() {

        if (!isEqualizerAvailable || equalizer1 != null) {
            return;
        }

        equalizer1 = new Equalizer(0, audioSessionId1);
        int i = equalizer1.setEnabled(true);
        Log.e("joymix", i + " test");


        equalizer2 = new Equalizer(0, audioSessionId2);
        equalizer2.setEnabled(true);

        virtualizer1 = new Virtualizer(0, audioSessionId1);
        virtualizer1.setEnabled(true);

        virtualizer2 = new Virtualizer(0, audioSessionId2);
        virtualizer2.setEnabled(true);

        bassBoost1 = new BassBoost(0, audioSessionId1);
        bassBoost1.setEnabled(true);

        bassBoost2 = new BassBoost(0, audioSessionId2);
        bassBoost2.setEnabled(true);

        applyDefaults();

    }

    private void applyDefaults() {

        hz60Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_60, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_60);
        hz100Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_100, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_100);
        hz240Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_240, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_240);

        hz500Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_500, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_500);
        hz1000Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_1000, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_1000);
        hz1800Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_1800, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_1800);

        hz3400Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_3400, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_3400);
        hz5700Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_5700, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_5700);
        hz9000Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_9000, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_9000);

        hz13000Level = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_13000, Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.DEFAULTS.HZ_13000);

        bassBoostStrength = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.KEYS.BASSBOOST_STRENGTH, Constants.AUDIO_SETTINGS.DEFAULTS.BASSBOOST_STRENGTH);
        virtualizerStrength = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.KEYS.VIRTUALIZER_STRENGTH, Constants.AUDIO_SETTINGS.DEFAULTS.VIRTUALIZER_STRENGTH);
        presetOption = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.KEYS.PRESET_OPTION, Constants.AUDIO_SETTINGS.DEFAULTS.PRESET_OPTION);
        trebleStrength = Utilities.getIntValueForKey(context, Constants.AUDIO_SETTINGS.KEYS.TREBLE_STRENGTH, Constants.AUDIO_SETTINGS.DEFAULTS.TREBLE_STRENGTH);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_60, hz60Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_100, hz100Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_240, hz240Level, true);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_500, hz500Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1000, hz1000Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1800, hz1800Level, true);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_3400, hz3400Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_5700, hz5700Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_9000, hz9000Level, true);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_13000, hz13000Level, true);

        setBassBoostStrength((short) bassBoostStrength, true);
        setVirtualizerStrength((short) virtualizerStrength, true);
        setTrebleStrength((short) trebleStrength, true);

    }

    public short levelToDb(int level) {
        short db = 0;
        if (level == 16) {
            db = 0;
        } else if (level < 16) {
            if (level == 0) {
                db = -1500;
            } else {
                db = (short) -((16 - level) * 100);
            }

        } else if (level > 16) {
            db = (short) ((level - 16) * 100);
        }
        return db;
    }

    private short checkBand(short band, int frequency) {

        if (band < 0 || band > equalizer1.getNumberOfBands()) {
            double percent = ((frequency - Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_60) / (double) (Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_13000 - Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_60));
            band = (short)(equalizer1.getNumberOfBands() * percent);
        }
        return band;

    }

    public void setEqualizerBandLevel(int frequency, int newLevel, Boolean shouldSave) {

        if (!isEqualizerAvailable) {
            return;
        }

        short band = equalizer1.getBand(frequency);
        band = checkBand(band, frequency);
        short db = levelToDb(newLevel);
        equalizer1.setBandLevel(band, db);
        equalizer2.setBandLevel(band, db);

        String key = null;
        if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_60) {
            hz60Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_60;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_100) {
            hz100Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_100;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_240) {
            hz240Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_240;
        }

        else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_500) {
            hz500Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_500;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1000) {
            hz1000Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_1000;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1800) {
            hz1800Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_1800;
        }

        else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_3400) {
            hz3400Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_3400;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_5700) {
            hz5700Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_5700;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_9000) {
            hz9000Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_9000;
        }

        else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_13000) {
            hz13000Level = newLevel;
            key = Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.KEYS.HZ_13000;
        }

        if (key != null && shouldSave) {
            Utilities.setIntValueForKey(context, newLevel, key);
        }

    }

    public int getEqualizerBandLevel(int frequency) {

        if (!isEqualizerAvailable) {
            return 16;
        }

        int level = 16;
        if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_60) {
            level = hz60Level;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_100) {
            level = hz100Level;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_240) {
            level = hz240Level;
        }

        else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_500) {
            level = hz500Level;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1000) {
            level = hz1000Level;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1800) {
            level = hz1800Level;
        }

        else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_3400) {
            level = hz3400Level;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_5700) {
            level = hz5700Level;
        }else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_9000) {
            level = hz9000Level;
        }

        else if (frequency == Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_13000) {
            level = hz13000Level;
        }
        return level;

    }


    public short getBassBoostStrength() {
        if (!isEqualizerAvailable) {
            return 0;
        }
        return bassBoost1.getRoundedStrength();
    }

    public void setBassBoostStrength(short strength, Boolean shouldSave) {
        if (!isEqualizerAvailable) {
            return;
        }
        bassBoost1.setStrength(strength);
        bassBoost2.setStrength(strength);
        bassBoostStrength = strength;
        if (shouldSave) {
            Utilities.setIntValueForKey(context, strength, Constants.AUDIO_SETTINGS.KEYS.BASSBOOST_STRENGTH);
        }
    }

    public short getVirtualizerStrength() {
        if (!isEqualizerAvailable) {
            return 0;
        }
        return virtualizer1.getRoundedStrength();
    }

    public void setVirtualizerStrength(short strength, Boolean shouldSave) {
        if (!isEqualizerAvailable) {
            return;
        }
        virtualizer1.setStrength(strength);
        virtualizer2.setStrength(strength);
        virtualizerStrength = strength;
        if (shouldSave) {
            Utilities.setIntValueForKey(context, strength, Constants.AUDIO_SETTINGS.KEYS.VIRTUALIZER_STRENGTH);
        }
    }


    public ArrayList<String> getPresetOptions() {
        return presetOptions;
    }

    private void adjustFrequenciesForPreset() {

        switch (presetOption) {
            case 1://Flat
                hz60Level = 16;
                hz100Level = 16;
                hz240Level = 16;

                hz500Level = 16;
                hz1000Level = 16;
                hz1800Level = 16;

                hz3400Level = 16;
                hz5700Level = 16;
                hz9000Level = 16;

                hz13000Level = 16;
                break;

            case 2://Rock (16+, 16-)
                hz60Level = 19;
                hz100Level = 7;
                hz240Level = 18;

                hz500Level = 14;
                hz1000Level = 10;
                hz1800Level = 10;

                hz3400Level = 14;
                hz5700Level = 20;
                hz9000Level = 20;

                hz13000Level = 19;
                break;

            case 3://Hip Hop (16+, 16-)
                hz60Level = 22;
                hz100Level = 19;
                hz240Level = 19;

                hz500Level = 17;
                hz1000Level = 16;
                hz1800Level = 16;

                hz3400Level = 18;
                hz5700Level = 18;
                hz9000Level = 20;

                hz13000Level = 20;
                break;

            case 4://Jazz (16+, 16-)
                hz60Level = 18;
                hz100Level = 22;
                hz240Level = 20;

                hz500Level = 16;
                hz1000Level = 10;
                hz1800Level = 14;

                hz3400Level = 20;
                hz5700Level = 14;
                hz9000Level = 14;

                hz13000Level = 18;
                break;

            case 5://Classical (16+, 16-)
                hz60Level = 18;
                hz100Level = 19;
                hz240Level = 20;

                hz500Level = 18;
                hz1000Level = 12;
                hz1800Level = 8;

                hz3400Level = 10;
                hz5700Level = 16;
                hz9000Level = 20;

                hz13000Level = 16;
                break;

            case 6://Heavy Metal (16+, 16-)
                hz60Level = 13;
                hz100Level = 19;
                hz240Level = 19;

                hz500Level = 13;
                hz1000Level = 11;
                hz1800Level = 11;

                hz3400Level = 13;
                hz5700Level = 17;
                hz9000Level = 21;

                hz13000Level = 19;
                break;

            case 7://Folk (16+, 16-)
                hz60Level = 20;
                hz100Level = 18;
                hz240Level = 16;

                hz500Level = 16;
                hz1000Level = 16;
                hz1800Level = 17;

                hz3400Level = 18;
                hz5700Level = 17;
                hz9000Level = 16;

                hz13000Level = 14;
                break;

            case 8://Dance (16+, 16-)
                hz60Level = 20;
                hz100Level = 20;
                hz240Level = 16;

                hz500Level = 10;
                hz1000Level = 14;
                hz1800Level = 18;

                hz3400Level = 14;
                hz5700Level = 16;
                hz9000Level = 18;

                hz13000Level = 18;
                break;

            case 9://Soft (16+, 16-) //Adjust this one
                hz60Level = 16;
                hz100Level = 16;
                hz240Level = 16;

                hz500Level = 16;
                hz1000Level = 16;
                hz1800Level = 16;

                hz3400Level = 16;
                hz5700Level = 16;
                hz9000Level = 16;

                hz13000Level = 16;
                break;

        }

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_60, hz60Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_100, hz100Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_240, hz240Level, true);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_500, hz500Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1000, hz1000Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_1800, hz1800Level, true);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_3400, hz3400Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_5700, hz5700Level, true);
        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_9000, hz9000Level, true);

        setEqualizerBandLevel(Constants.AUDIO_SETTINGS.EQUALIZER_BANDS.FREQUENCIES.HZ_13000, hz13000Level, true);

    }

    public void setPresetOption(int option) {

        presetOption = option;
        adjustFrequenciesForPreset();
        Utilities.setIntValueForKey(context, presetOption, Constants.AUDIO_SETTINGS.KEYS.PRESET_OPTION);

    }

    public void disablePreset() {

        presetOption = 0;
        Utilities.setIntValueForKey(context, presetOption, Constants.AUDIO_SETTINGS.KEYS.PRESET_OPTION);

    }

    public int getPresetOption() {
        return presetOption;
    }

    public void setVolume(int volume) {
        volume = (int) (volume / 1000.0 * maxVolume);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    public int getVolume() {
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volume = (int) ((float)volume / maxVolume * 1000);
        return volume;
    }

    public void releaseEqualizer() {
        if (!isEqualizerAvailable) {
            return;
        }
        equalizer1.release();
        equalizer2.release();
        virtualizer1.release();
        virtualizer2.release();
        bassBoost1.release();
        bassBoost2.release();
        equalizer1 = null;
        equalizer2 = null;
        virtualizer1 = null;
        virtualizer2 = null;
        bassBoost1 = null;
        bassBoost2 = null;
    }

    public void setTrebleStrength(int strength, Boolean shouldSave) {

        if (!isEqualizerAvailable) {
            return;
        }

        //Converting 0-1000 range to 0-31
        int level = (int) (31.0 / 1000.0 * strength);

        short band = (short) (equalizer1.getNumberOfBands() - 1);
        if (band  < 0) {
            return;
        }
        short db = levelToDb(level);
        equalizer1.setBandLevel(band, db);
        equalizer2.setBandLevel(band, db);

        trebleStrength = strength;
        if (shouldSave) {
            Utilities.setIntValueForKey(context, strength, Constants.AUDIO_SETTINGS.KEYS.TREBLE_STRENGTH);
        }

    }

    public int getTrebleStrength() {
        return trebleStrength;
    }

}
