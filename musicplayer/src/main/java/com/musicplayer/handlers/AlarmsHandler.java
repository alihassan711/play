package com.musicplayer.handlers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Alarm;
import com.musicplayer.models.Album;
import com.musicplayer.models.Artist;
import com.musicplayer.models.Playlist;
import com.musicplayer.models.Track;
import com.musicplayer.receivers.CustomAlarmReceiver;
import com.musicplayer.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by umair on 11/13/16.
 */
public class AlarmsHandler {

    private ArrayList<Alarm> alarms = null;
    private AlarmManager alarmManager;

    public AlarmsHandler() {

    }

    public void setAlarm(Track track, long time) {
        Alarm alarm = new Alarm(track.getTitle() +  " (Track)", time, track.getId(), -1, -1);
        setAlarm(alarm);
    }

    public void setAlarm(Playlist playlist, long time) {
        Alarm alarm = new Alarm(playlist.getTitle() + " (Playlist)", time, -1, playlist.getId(), Constants.DATABASE.CUSTOM_Q_TYPES.get("Playlist"));
        setAlarm(alarm);
    }

    public void setAlarm(Album album, long time) {
        Alarm alarm = new Alarm(album.getTitle() + " (Album)", time, -1, album.getId(), Constants.DATABASE.CUSTOM_Q_TYPES.get("Album"));
        setAlarm(alarm);
    }

    public void setAlarm(Artist artist, long time) {
        Alarm alarm = new Alarm(artist.getTitle() + " (Artist)", time, -1, artist.getId(), Constants.DATABASE.CUSTOM_Q_TYPES.get("Artist"));
        setAlarm(alarm);
    }

    public void updateAlarm(Alarm alarm) {

        Context context = MusicPlayer.getSharedInstance().getContext();
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }

        Intent intent = new Intent(context, CustomAlarmReceiver.class);
        intent.putExtra("trackId", alarm.getTrackId());
        intent.putExtra("customQId", alarm.getCustomQId());
        intent.putExtra("customQType", alarm.getCustomQType());
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, (int) alarm.getId(), intent, 0);

        alarmManager.cancel(alarmIntent);

        alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTime(), alarmIntent);

        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).updateAlarm(alarm);
        sort();

    }

    public void deleteAlarm(Alarm alarm) {

        Context context = MusicPlayer.getSharedInstance().getContext();
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }

        Intent intent = new Intent(context, CustomAlarmReceiver.class);
        intent.putExtra("trackId", alarm.getTrackId());
        intent.putExtra("customQId", alarm.getCustomQId());
        intent.putExtra("customQType", alarm.getCustomQType());
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, (int) alarm.getId(), intent, 0);

        alarmManager.cancel(alarmIntent);

        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).deleteAlarm(alarm);
        alarms.remove(alarm);
        sort();

    }

    private void setAlarm(Alarm alarm) {

        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).saveAlarm(alarm);
        Context context = MusicPlayer.getSharedInstance().getContext();
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        Intent intent = new Intent(context, CustomAlarmReceiver.class);
        intent.putExtra("trackId", alarm.getTrackId());
        intent.putExtra("customQId", alarm.getCustomQId());
        intent.putExtra("customQType", alarm.getCustomQType());
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, (int) alarm.getId(), intent, 0);

        alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTime(), alarmIntent);

        if (alarms == null) {
            alarms = new ArrayList<>();
        }
        alarms.add(alarm);
        sort();

    }

    public ArrayList<Alarm> getAlarms() {
        long timeFilter = System.currentTimeMillis();
        DatabaseHandler databaseHandler = DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext());
        if (alarms == null) {
            alarms = databaseHandler.loadAlarms(timeFilter);
            sort();
        }else {
            for (int i = 0; i < alarms.size(); ) {
                if (alarms.get(i).getTime() <= timeFilter) {
                    alarms.remove(i);
                }else {
                    i++;
                }
            }
        }
        databaseHandler.deleteAlarms(timeFilter);

        return alarms;
    }

    private void sort() {
        Collections.sort(alarms, new Comparator<Alarm>() {
            @Override
            public int compare(Alarm lhs, Alarm rhs) {
                long l1 = lhs.getTime(), l2 = rhs.getTime();
                return l1 > l2 ? 1: l1 < l2 ? -1 : 0;
            }
        });
    }

    public void setSleeper(long time) {

        Context context = MusicPlayer.getSharedInstance().getContext();
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }

        Intent intent = new Intent(context, CustomAlarmReceiver.class);
        intent.putExtra("sleeperId", 101);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, -101, intent, 0);

        alarmManager.cancel(alarmIntent);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, alarmIntent);

    }

    public void cancelSleeper() {

        Context context = MusicPlayer.getSharedInstance().getContext();
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }

        Intent intent = new Intent(context, CustomAlarmReceiver.class);
        intent.putExtra("sleeperId", 101);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, -101, intent, 0);

        alarmManager.cancel(alarmIntent);

    }


}
