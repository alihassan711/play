package com.musicplayer.handlers;

import android.content.Context;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Genre;
import com.musicplayer.models.Playlist;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.util.ArrayList;

/**
 * Created by umair on 11/13/16.
 */
public class GenresHandler extends BaseHandler {

    private ArrayList<Genre> genres = new ArrayList<>();

    public GenresHandler() {

    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public Boolean addGenre(Genre genre) {

        if (genres.contains(genre)) {
            return false;
        }

        for (Genre gnr : genres) {
            if (gnr.getTitle().equalsIgnoreCase(genre.getTitle())) {
                return  false;
            }
        }

        genres.add(genre);
        return true;

    }

    public void changeGenreTitle(Genre genre, String newTitle) {
        genre.setTitle(newTitle);
        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).changeTitleCustomQ(genre);
        sortGenres(Constants.SORT_ORDER.A_TO_Z);
    }

    public void sortGenres(String sortOrder) {
        ArrayList<Genre> tmpGenres = new ArrayList<>();
        for (int i = 1; i < genres.size();) {
            Genre genre = genres.get(i);
            tmpGenres.add(genre);
            genres.remove(genre);
        }
        sortCustomQs(tmpGenres, sortOrder);
        for (Genre genre : tmpGenres) {
            genres.add(genre);
        }


    }

    public int getGenreIndexContainingTrack(Track track) {

        for (int i = 1; i < genres.size(); i++) {
            if (genres.get(i).getTracksList().contains(track)) {
                return i;
            }
        }
        if (genres.get(0).getTracksList().contains(track)) {
            return 0;
        }
        return -1;
    }

    public void deleteGenre(Genre genre) {

        if (genres.indexOf(genre) > 0) {
            Context context = MusicPlayer.getSharedInstance().getContext();
            DatabaseHandler.getSharedInstance(context).deleteCustomQ(genre);
            genres.remove(genre);
            sortGenres(Constants.SORT_ORDER.A_TO_Z);
        }

    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        for (Genre genre : genres) {
            genre.refreshFilteredTracks(shouldFilter);
        }
    }

    public Genre getGenerWithId(long genreId) {
        for (Genre genre : genres) {
            if (genre.getId() == genreId) {
                return genre;
            }
        }
        return null;
    }

}
