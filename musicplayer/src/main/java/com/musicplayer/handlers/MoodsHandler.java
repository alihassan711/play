package com.musicplayer.handlers;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Mood;
import com.musicplayer.models.SegmentedTrack;
import com.musicplayer.models.Track;

import java.util.ArrayList;

/**
 * Created by umair on 11/13/16.
 */
public class MoodsHandler extends BaseHandler {

    private ArrayList<Mood> moods = new ArrayList<>();

    public MoodsHandler() {

    }

    public void loadDefaults() {
        moods.add(new Mood(2, "Party"));
        moods.add(new Mood(3, "Inspire"));
        moods.add(new Mood(4, "Love"));
        moods.add(new Mood(5, "Sad"));

        moods.add(new Mood(6, "Relax"));
        moods.add(new Mood(7, "Gaming"));
        moods.add(new Mood(8, "Workout"));
        moods.add(new Mood(9, "Joymix"));
    }

    public ArrayList<String> getMoodsTitles() {
        ArrayList<String> titles = new ArrayList<>();
        for (Mood mood : moods) {
            titles.add(mood.getTitle());
        }
        return titles;
    }

    public ArrayList<Mood> getMoods() {
        return moods;
    }

    public void sortMoods(String sortOrder) {
        sortCustomQs(moods, sortOrder);
    }

    public ArrayList<Mood> getMoodsWithIndices(ArrayList<Integer> indices) {
        ArrayList<Mood> result = new ArrayList<>();
        for (Integer index : indices) {
            if (index < moods.size()) {
                result.add(moods.get(index));
            }
        }
        return result;
    }

    public Mood getJoymix() {
        for (Mood mood : moods) {
            if (mood.getTitle().toLowerCase().equals("joymix")) {
                return mood;
            }
        }
        return null;
    }

    public void saveSegmentedTrackToJoymix(Track track, long startTime, long endTime) {

        Mood joymix = getJoymix();
        if (joymix == null) {
            return;
        }
        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).saveSegmentedTrack(track, startTime, endTime);
        SegmentedTrack segmentedTrack = new SegmentedTrack(track, startTime, endTime);
        joymix.addTrack(segmentedTrack);

    }

    public Mood getMoodWithIndex(int index) {
        if (index >= 0 && index < moods.size()) {
            return moods.get(index);
        }
        return null;
    }

    public Mood getMoodWithTitle(String title) {
        for (Mood mood : moods) {
            if (mood.getTitle().equalsIgnoreCase(title))
            return mood;
        }
        return null;
    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        for (Mood mood : moods) {
            mood.refreshFilteredTracks(shouldFilter);
        }
    }

    public Mood getMoodWithId(long moodId) {
        for (Mood mood : moods) {
            if (mood.getId() == moodId) {
                return mood;
            }
        }
        return null;
    }

    public Mood getMoodForTrack(Track toSearch) {
        for (Mood mood : moods) {
            for (Track moodTrack : mood.getTracksList()) {
                if (moodTrack.getTitle().equalsIgnoreCase(toSearch.getTitle())) {
                    return mood;
                }
            }
        }
        return null;
    }

    public boolean areTracksAddedInMoods() {
        for (Mood mood : moods) {
            if (mood.getTracksList().size() > 0) {
                return true;
            }
        }
        return false;
    }

}
