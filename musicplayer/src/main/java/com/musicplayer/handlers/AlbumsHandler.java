package com.musicplayer.handlers;

import android.content.Context;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.models.Album;
import com.musicplayer.models.Track;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umair on 11/13/16.
 */
public class AlbumsHandler extends BaseHandler {

    private ArrayList<Album> albums = new ArrayList<>();

    public AlbumsHandler() {

    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public Boolean addAlbum(Album album) {

        if (albums.contains(album)) {
            return false;
        }

        for (Album alb : albums) {
            if (alb.getTitle().equalsIgnoreCase(album.getTitle())) {
                return  false;
            }
        }

        albums.add(album);
        return true;

    }

    public void sortAlbums(String sortOrder) {
        sortCustomQs(albums, sortOrder);
        Utilities.saveValueForKey(MusicPlayer.getSharedInstance().getContext(), sortOrder, Constants.PREFERENCES.KEYS.ALBUMS_SORT_ORDER);
    }

    private void sortWithCurrentOrder() {
        String albumsSortOrder = Utilities.getValueForKey(MusicPlayer.getSharedInstance().getContext(), Constants.PREFERENCES.KEYS.ALBUMS_SORT_ORDER);
        if (albumsSortOrder == null) {
            albumsSortOrder = Constants.PREFERENCES.DEFAULTS.ALBUMS_SORT_ORDER;
        }
        sortAlbums(albumsSortOrder);
    }

    public void deleteAlbum(Album album) {

        List<Track> tracks = album.getAllTracks();
        for (int  i = 0; i < tracks.size();) {
            MusicPlayer.getSharedInstance().getTracksHandler().deleteTrack(tracks.get(i));
        }

        Context context = MusicPlayer.getSharedInstance().getContext();
        DatabaseHandler.getSharedInstance(context).deleteCustomQ(album);
        albums.remove(album);
        sortWithCurrentOrder();
    }

    public void changeAlbumTitle(Album album, String newTitle) {
        album.setTitle(newTitle);
        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).changeTitleCustomQ(album);
        sortWithCurrentOrder();
    }

    public ArrayList<Album> getAlbumsWithIndices(ArrayList<Integer> indices) {
        ArrayList<Album> result = new ArrayList<>();
        for (Integer index : indices) {
            if (index < albums.size()) {
                result.add(albums.get(index));
            }
        }
        return result;
    }

    public void deleteAlbumsWithIndices(ArrayList<Integer> indices) {

        for (Integer index : indices) {
            if (index < albums.size()) {
                deleteAlbum(albums.get(index));
            }
        }
        sortWithCurrentOrder();

    }

    public Album getAlbumContainingTrack(Track trackToSearch) {
        for (Album album : albums) {
            for (Track track : album.getTracksList()) {
                if (track.getId() == trackToSearch.getId()) {
                    return album;
                }
            }
        }
        return null;
    }

    public Album getAlbumWithId(long albumId) {
        for (Album album : albums) {
            if (album.getId() == albumId) {
                return album;
            }
        }
        return null;
    }

    public ArrayList<Album> filterAlbums(String text) {
        if (text.isEmpty()) {
            return albums;
        }
        ArrayList<Album> tmpAlbums = new ArrayList<>();
        for (Album album : albums) {
            if (album.getTitle().toLowerCase().startsWith(text)) {
                tmpAlbums.add(album);
            }
        }
        return tmpAlbums;
    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        for (Album album : albums) {
            album.refreshFilteredTracks(shouldFilter);
        }
    }

}
