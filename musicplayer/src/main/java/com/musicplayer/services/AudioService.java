package com.musicplayer.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RemoteControlClient;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.musicplayer.MusicPlayer;
import com.musicplayer.R;
import com.musicplayer.models.SegmentedTrack;
import com.musicplayer.models.Track;
import com.musicplayer.receivers.NotificationReceiver;
import com.musicplayer.utils.Constants;
import com.musicplayer.utils.Utilities;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static android.R.attr.data;

/**
 * Created by umair on 11/7/16.
 */
public class AudioService extends Service implements AudioManager.OnAudioFocusChangeListener {

    MediaPlayer mediaPlayer1, mediaPlayer2;
    MediaPlayer currentMediaPlayer, otherMediaPlayer;
    Timer timer;
    private Boolean currentVersionSupportBigNotification = false, currentVersionSupportLockScreenControls = false;
    private ComponentName remoteComponentName;
    private RemoteControlClient remoteControlClient;
    private AudioManager audioManager;
    private boolean audioFocusGained = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mediaPlayer1 = new MediaPlayer();
        mediaPlayer2 = new MediaPlayer();

        mediaPlayer1.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer2.setAudioStreamType(AudioManager.STREAM_MUSIC);

        mediaPlayer1.setOnCompletionListener(onCompletionListener);
        mediaPlayer2.setOnCompletionListener(onCompletionListener);

        timer = new Timer();
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        currentVersionSupportBigNotification = Utilities.currentVersionSupportBigNotification();
        currentVersionSupportLockScreenControls = Utilities.currentVersionSupportLockScreenControls();

        currentMediaPlayer = mediaPlayer1;
        otherMediaPlayer = mediaPlayer2;

        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        MusicPlayer.getSharedInstance().setMediaHandler(new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                String action = (String) msg.obj;
                if (action.equalsIgnoreCase("play")) {
                    play();
                } else if (action.equalsIgnoreCase("pause")) {
                    pause();
                } else if (action.equalsIgnoreCase("prepare")) {
                    prepareTrack();
                    play();
                } else if (action.equalsIgnoreCase("changeCurrent")) {
                    int currentPosition = msg.arg1;
                    currentMediaPlayer.seekTo(currentPosition);
                }
                return false;
            }
        }));

        MusicPlayer.getSharedInstance().createAudioService(mediaPlayer1, mediaPlayer2);

        prepareTrack();
        play();

//        if(currentVersionSupportLockScreenControls){
//            registerRemoteClient();
//        }

        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {
        if(mediaPlayer1 != null){
            mediaPlayer1.stop();
            mediaPlayer1 = null;
        }
        if(mediaPlayer2 != null){
            mediaPlayer2.stop();
            mediaPlayer2 = null;
        }
        stopForeground(true);
        abandonAudioFocus();
        super.onDestroy();
    }
    

    private void prepareTrack() {

        Track currentTrack = MusicPlayer.getSharedInstance().getCurrentTrack();
        Track nextTrack = MusicPlayer.getSharedInstance().getNextTrack();

        if (currentTrack == null || nextTrack == null) {
            return;
        }
        MusicPlayer.getSharedInstance().onTrackChange(currentTrack);

        try {

            currentMediaPlayer.reset();
            currentMediaPlayer.setDataSource(currentTrack.getStoragePath());
            currentMediaPlayer.prepare();

            otherMediaPlayer.reset();
            otherMediaPlayer.setDataSource(nextTrack.getStoragePath());
            otherMediaPlayer.prepare();

            timer.scheduleAtFixedRate(new ProgressTask(), 0, 1000);
            if (currentTrack instanceof SegmentedTrack) {
                currentMediaPlayer.seekTo((int) ((SegmentedTrack)currentTrack).getStartTime());
            }
            if (nextTrack instanceof SegmentedTrack) {
                otherMediaPlayer.seekTo((int) ((SegmentedTrack)nextTrack).getStartTime());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
//        if(currentVersionSupportLockScreenControls && remoteControlClient != null){
//            updateMetadata();
//            remoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
//        }

    }

    private void play() {
        if (MusicPlayer.getSharedInstance().isPlaying() && requestAudioFocus()) {
            currentMediaPlayer.start();
            MusicPlayer.getSharedInstance().onPlayPauseStateChange();
            sendNotification();
            Track currentTrack = MusicPlayer.getSharedInstance().getCurrentTrack();
            Context context = MusicPlayer.getSharedInstance().getContext();
            if (currentTrack != null && context != null) {
                Utilities.setIntValueForKey(context, (int) currentTrack.getId(), Constants.KEYS.LAST_PLAYING_TRACK_ID);
            }
        }
    }

    private void pause() {
        currentMediaPlayer.pause();
        MusicPlayer.getSharedInstance().onPlayPauseStateChange();
        sendNotification();
//        stopForeground(true);
    }

    private void toggleMediaPlayers() {

        if (currentMediaPlayer == mediaPlayer1) {
            currentMediaPlayer = mediaPlayer2;
            otherMediaPlayer = mediaPlayer1;
        }else {
            currentMediaPlayer = mediaPlayer1;
            otherMediaPlayer = mediaPlayer2;
        }

    }

    private void prepareNextTrackAfter() {

        Track nextTrack = MusicPlayer.getSharedInstance().getNextTrack();
        if (nextTrack == null) {
            return;
        }

        try {

            otherMediaPlayer.reset();
            otherMediaPlayer.setDataSource(nextTrack.getStoragePath());
            otherMediaPlayer.prepare();

            if (nextTrack instanceof SegmentedTrack) {
                otherMediaPlayer.seekTo((int) ((SegmentedTrack)nextTrack).getStartTime());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        Track currentTrack = MusicPlayer.getSharedInstance().getCurrentTrack();
        if (currentTrack == null) {
            return;
        }
        MusicPlayer.getSharedInstance().onTrackChange(currentTrack);

    }

    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (MusicPlayer.getSharedInstance().isShuffleOn()) {
                MusicPlayer.getSharedInstance().playRandomTrack();
            }else if (MusicPlayer.getSharedInstance().getRepeat() == 1) {
                currentMediaPlayer.seekTo(0);
                currentMediaPlayer.start();
                MusicPlayer.getSharedInstance().updatePlayCountAndTime(MusicPlayer.getSharedInstance().getCurrentTrack());
            }else if (MusicPlayer.getSharedInstance().getRepeat() == 2) {
                toggleMediaPlayers();
                MusicPlayer.getSharedInstance().playNextTrackWithoutPrepare();
                prepareNextTrackAfter();
            }else {
                if (!MusicPlayer.getSharedInstance().isCurrentTrackLast()) {
                    toggleMediaPlayers();
                    MusicPlayer.getSharedInstance().playNextTrackWithoutPrepare();
                    prepareNextTrackAfter();
                }
            }
        }
    };

    private class ProgressTask extends TimerTask {

        @Override
        public void run() {
            progressHandler.sendMessage(progressHandler.obtainMessage());
        }
    }

    private final Handler progressHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if(currentMediaPlayer != null){
                try {

                    MusicPlayer musicPlayer = MusicPlayer.getSharedInstance();

                    musicPlayer.onTrackCurrentPositionChange(currentMediaPlayer.getCurrentPosition());
                    Track track = musicPlayer.getCurrentTrack();

                    if (track instanceof SegmentedTrack) {

                        if (((SegmentedTrack)track).getEndTime() <= currentMediaPlayer.getCurrentPosition()) {
                            musicPlayer.playNextTrack();
                        }

                    }else {

                        if (musicPlayer.isCrossFadeOn() && currentMediaPlayer.isPlaying()) {

                            long remainingTime = Math.abs(currentMediaPlayer.getCurrentPosition() - track.getDuration());
                            if (remainingTime < musicPlayer.getFadeDelta()) {
                                if (!otherMediaPlayer.isPlaying()) {
                                    otherMediaPlayer.start();
                                }

                                float volume = ((float)remainingTime) / musicPlayer.getFadeDelta();
                                currentMediaPlayer.setVolume(volume, volume);
                                volume = 1 - volume;
                                otherMediaPlayer.setVolume(volume, volume);

                            }

                        }else if (musicPlayer.isFadeOutInOn() && currentMediaPlayer.isPlaying()) {

                            long remainingTime = Math.abs(currentMediaPlayer.getCurrentPosition() - track.getDuration());
                            if (remainingTime < musicPlayer.getFadeDelta()) {
                                float volume = ((float)remainingTime) / musicPlayer.getFadeDelta();
                                currentMediaPlayer.setVolume(volume, volume);
                            }else if (currentMediaPlayer.getCurrentPosition() < musicPlayer.getFadeDelta()) {
                                float volume = ((float)currentMediaPlayer.getCurrentPosition()) / musicPlayer.getFadeDelta();
                                currentMediaPlayer.setVolume(volume, volume);
                            }

                        }

                    }
                }catch(Exception e){}
            }
        }
    };

    @SuppressLint("NewApi")
    private void sendNotification() {

        Track track = MusicPlayer.getSharedInstance().getCurrentTrack();
        if (track == null) {
            return;
        }

        RemoteViews simpleContentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.custom_notification);
        RemoteViews expandedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.big_notification);

        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(track.getTitle())
                .setPriority(Notification.PRIORITY_MAX)
                .build();
        setNotificationListeners(simpleContentView);
        setNotificationListeners(expandedView);

        notification.contentView = simpleContentView;
        if(currentVersionSupportBigNotification){
            notification.bigContentView = expandedView;
        }

        Bitmap albumArt = null;
        if (track.getThumbImgStoragePath() != null && !track.getThumbImgStoragePath().isEmpty()) {
            albumArt = Utilities.getLocalFileBitmap(track.getThumbImgStoragePath());
        }
        if (albumArt == null){
            albumArt = MusicPlayer.getSharedInstance().getAlbumArtForTrack(track, true);
        }
        notification.contentView.setImageViewBitmap(R.id.imgAlbumArt, albumArt);
        if(currentVersionSupportBigNotification){
            notification.bigContentView.setImageViewBitmap(R.id.imgAlbumArt, albumArt);
        }

        if(!MusicPlayer.getSharedInstance().isPlaying()){

            notification.contentView.setViewVisibility(R.id.btnPause, View.GONE);
            notification.contentView.setViewVisibility(R.id.btnPlay, View.VISIBLE);

            if(currentVersionSupportBigNotification){
                notification.bigContentView.setViewVisibility(R.id.btnPause, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.btnPlay, View.VISIBLE);
            }

        }else{

            notification.contentView.setViewVisibility(R.id.btnPause, View.VISIBLE);
            notification.contentView.setViewVisibility(R.id.btnPlay, View.GONE);

            if(currentVersionSupportBigNotification){
                notification.bigContentView.setViewVisibility(R.id.btnPause, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.btnPlay, View.GONE);
            }

        }

        notification.contentView.setTextViewText(R.id.txtTitle, track.getTitle());
        notification.contentView.setTextViewText(R.id.txtAlbum, track.getAlbum());
        if(currentVersionSupportBigNotification){
            notification.bigContentView.setTextViewText(R.id.txtTitle, track.getTitle());
            notification.bigContentView.setTextViewText(R.id.txtArtist, track.getArtist());
            notification.bigContentView.setTextViewText(R.id.txtAlbum, track.getAlbum());
        }

        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        startForeground(Constants.NOTIFICATION.ID, notification);

    }

    public void setNotificationListeners(RemoteViews view) {

        Intent previous = new Intent(Constants.NOTIFICATION.NOTIFY_PREVIOUS);
        Intent pause = new Intent(Constants.NOTIFICATION.NOTIFY_PAUSE);
        Intent play = new Intent(Constants.NOTIFICATION.NOTIFY_PLAY);
        Intent next = new Intent(Constants.NOTIFICATION.NOTIFY_NEXT);
        Intent close = new Intent(Constants.NOTIFICATION.NOTIFY_CLOSE);
        Intent openApp = new Intent(Constants.NOTIFICATION.NOTIFY_OPEN_APP);

        PendingIntent pPrevious = PendingIntent.getBroadcast(getApplicationContext(), 0, previous, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPrevious, pPrevious);

        PendingIntent pPause = PendingIntent.getBroadcast(getApplicationContext(), 0, pause, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPause, pPause);

        PendingIntent pPlay = PendingIntent.getBroadcast(getApplicationContext(), 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPlay, pPlay);

        PendingIntent pNext = PendingIntent.getBroadcast(getApplicationContext(), 0, next, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnNext, pNext);

        PendingIntent pClose = PendingIntent.getBroadcast(getApplicationContext(), 0, close, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnClose, pClose);

        PendingIntent pOpenApp = PendingIntent.getBroadcast(getApplicationContext(), 0, openApp, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.container, pOpenApp);

    }


    private void registerRemoteClient(){
        remoteComponentName = new ComponentName(getApplicationContext(), new NotificationReceiver().componentName());
        try {
            if(remoteControlClient == null) {
                audioManager.registerMediaButtonEventReceiver(remoteComponentName);
                Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                mediaButtonIntent.setComponent(remoteComponentName);
                PendingIntent mediaPendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0);
                remoteControlClient = new RemoteControlClient(mediaPendingIntent);
                audioManager.registerRemoteControlClient(remoteControlClient);
            }
            remoteControlClient.setTransportControlFlags(
                    RemoteControlClient.FLAG_KEY_MEDIA_PLAY |
                            RemoteControlClient.FLAG_KEY_MEDIA_PAUSE |
                            RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE |
                            RemoteControlClient.FLAG_KEY_MEDIA_STOP |
                            RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS |
                            RemoteControlClient.FLAG_KEY_MEDIA_NEXT);
        }catch(Exception ex) {
        }
    }

    private void updateMetadata(){

        if (remoteControlClient == null) {
            return;
        }

        Track track = MusicPlayer.getSharedInstance().getCurrentTrack();
        if (track == null) {
            return;
        }

        RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ALBUM, track.getAlbum());
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ARTIST, track.getArtist());
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_TITLE, track.getTitle());
        Bitmap albumArt = MusicPlayer.getSharedInstance().getAlbumArtForTrack(track);
        metadataEditor.putBitmap(RemoteControlClient.MetadataEditor.BITMAP_KEY_ARTWORK, albumArt);
        metadataEditor.apply();
        audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

    }


    private boolean requestAudioFocus() {
        if (!audioFocusGained && MusicPlayer.getSharedInstance().getContext() != null) {
            AudioManager audioManager = (AudioManager) MusicPlayer.getSharedInstance().getContext().getSystemService(Context.AUDIO_SERVICE);
            int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            audioFocusGained = result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
        }
        return audioFocusGained;
    }

    private void abandonAudioFocus() {
        if (audioFocusGained && MusicPlayer.getSharedInstance().getContext() != null) {
            AudioManager audioManager = (AudioManager) MusicPlayer.getSharedInstance().getContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.abandonAudioFocus(this);
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            audioFocusGained = false;
            MusicPlayer.getSharedInstance().pause();
        }
        else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
            audioFocusGained = false;
            MusicPlayer.getSharedInstance().pause();
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
            audioFocusGained = true;
            if (MusicPlayer.getSharedInstance().isPlaying()) {
                MusicPlayer.getSharedInstance().play();
            }
        }
    }

}
