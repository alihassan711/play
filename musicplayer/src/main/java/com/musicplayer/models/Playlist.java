package com.musicplayer.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by umair on 11/5/16.
 */
public class Playlist extends CustomQ {

    public Playlist() {
    }

    public Playlist(long id, String title) {
        this.id = id;
        this.title = title;
        this.createdAt = new Date().getTime();
    }

    public void rename(String newTitle) {
        title = newTitle;
    }

    public void addArtist(Artist artist) {
        for(Track track : artist.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addAlbum(Album album) {
        for(Track track : album.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addGenre(Genre genre) {
        for(Track track : genre.getAllTracks()) {
            addTrack(track);
        }
    }

}
