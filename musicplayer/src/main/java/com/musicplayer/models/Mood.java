package com.musicplayer.models;

import java.util.ArrayList;

/**
 * Created by umair on 11/5/16.
 */
public class Mood extends CustomQ {

    public Mood() {
    }

    public Mood(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public void addPlaylist(Playlist playlist) {
        for(Track track : playlist.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addArtist(Artist artist) {
        for(Track track : artist.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addAlbum(Album album) {
        for(Track track : album.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addGenre(Genre genre) {
        for(Track track : genre.getAllTracks()) {
            addTrack(track);
        }
    }


}
