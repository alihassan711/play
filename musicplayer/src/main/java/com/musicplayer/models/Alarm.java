package com.musicplayer.models;

/**
 * Created by umair on 11/5/16.
 */
public class Alarm {

    private long id, time, trackId, customQId;
    private int customQType;
    private String title;

    public Alarm(){
    }

    public Alarm(String title, long time, long trackId, long customQId, int customQType) {
        this.title = title;
        this.time = time;
        this.trackId = trackId;
        this.customQId = customQId;
        this.customQType = customQType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getTrackId() {
        return trackId;
    }

    public void setTrackId(long trackId) {
        this.trackId = trackId;
    }

    public long getCustomQId() {
        return customQId;
    }

    public void setCustomQId(long customQId) {
        this.customQId = customQId;
    }

    public int getCustomQType() {
        return customQType;
    }

    public void setCustomQType(int customQType) {
        this.customQType = customQType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String toString() {
        return String.valueOf(id) + ": " + title + ":";
    }

}
