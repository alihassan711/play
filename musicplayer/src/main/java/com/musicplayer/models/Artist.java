package com.musicplayer.models;

/**
 * Created by umair on 11/5/16.
 */
public class Artist extends CustomQ {

    public Artist() {

    }

    public Artist(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public void rename(String newTitle) {
        title = newTitle;
    }

}
