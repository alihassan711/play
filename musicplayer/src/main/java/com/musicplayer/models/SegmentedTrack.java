package com.musicplayer.models;

/**
 * Created by umair on 11/19/16.
 */

public class SegmentedTrack extends Track {

    private long startTime, endTime;

    public SegmentedTrack(Track track, long startTime, long endTime) {
        super(track.getId(), track.getDuration(), track.getYear(), track.getPlayCount(),
                track.getCreatedAt(), track.getLastPlayed(), track.getAlbumId(), track.getStorageId(),
                track.getTitle(), track.getStoragePath(), track.getAlbum(), track.getArtist());
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
