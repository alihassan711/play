package com.musicplayer.models;

/**
 * Created by umair on 11/5/16.
 */
public class MyQ extends CustomQ {

    public MyQ() {
        id = 99999;
        title = "My Q";
    }

    public void addArtist(Artist artist) {
        for(Track track : artist.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addAlbum(Album album) {
        for(Track track : album.getAllTracks()) {
            addTrack(track);
        }
    }

    public void addGenre(Genre genre) {
        for(Track track : genre.getAllTracks()) {
            addTrack(track);
        }
    }

}
