package com.musicplayer.models;

/**
 * Created by umair on 11/5/16.
 */
public class Track {

    private long id, duration, year, playCount, createdAt, lastPlayed, albumId;
    private int storageId;
    private String title = "", storagePath = "", album = "", artist = "";
    private String youtubeTitle = "", youtubeThumbUrl = "", youtubeVideoUrl = "";
    private String thumbImgUrl = "", fullImgUrl = "";
    private String thumbImgStoragePath = "", fullImgStoragePath = "";

    public Track(){
    }

    public Track(long id, long duration, long year, long playCount, long createdAt, long lastPlayed, long albumId, int storageId, String title, String storagePath, String album, String artist) {
        this.id = id;
        this.duration = duration;
        this.year = year;
        this.playCount = playCount;
        this.createdAt = createdAt;
        this.lastPlayed = lastPlayed;
        this.albumId = albumId;
        this.storageId = storageId;
        this.title = title;
        this.storagePath = storagePath;
        this.album = album;
        this.artist = artist;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getPlayCount() {
        return playCount;
    }

    public void setPlayCount(long playCount) {
        this.playCount = playCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getLastPlayed() {
        return lastPlayed;
    }

    public void setLastPlayed(long lastPlayed) {
        this.lastPlayed = lastPlayed;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public int getStorageId() {
        return storageId;
    }

    public void setStorageId(int storageId) {
        this.storageId = storageId;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public String getYoutubeTitle() {
        return youtubeTitle;
    }

    public void setYoutubeTitle(String youtubeTitle) {
        this.youtubeTitle = youtubeTitle;
    }

    public String getYoutubeThumbUrl() {
        return youtubeThumbUrl;
    }

    public void setYoutubeThumbUrl(String youtubeThumbUrl) {
        this.youtubeThumbUrl = youtubeThumbUrl;
    }

    public String getYoutubeVideoUrl() {
        return youtubeVideoUrl;
    }

    public void setYoutubeVideoUrl(String youtubeVideoUrl) {
        this.youtubeVideoUrl = youtubeVideoUrl;
    }

    public String getThumbImgUrl() {
        return thumbImgUrl;
    }

    public void setThumbImgUrl(String thumbImgUrl) {
        this.thumbImgUrl = thumbImgUrl;
    }

    public String getFullImgUrl() {
        return fullImgUrl;
    }

    public void setFullImgUrl(String fullImgUrl) {
        this.fullImgUrl = fullImgUrl;
    }

    public String getThumbImgStoragePath() {
        return thumbImgStoragePath;
    }

    public void setThumbImgStoragePath(String thumbImgStoragePath) {
        this.thumbImgStoragePath = thumbImgStoragePath;
    }

    public String getFullImgStoragePath() {
        return fullImgStoragePath;
    }

    public void setFullImgStoragePath(String fullImgStoragePath) {
        this.fullImgStoragePath = fullImgStoragePath;
    }

    public String toString() {
        return String.valueOf(id) + ": " + title + ":";
    }

}
