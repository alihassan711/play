package com.musicplayer.models;

import android.os.AsyncTask;
import android.util.Log;

import com.musicplayer.MusicPlayer;
import com.musicplayer.database.DatabaseHandler;
import com.musicplayer.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by umair on 11/5/16.
 */
public class CustomQ {

    public interface OnSortCompleteCallback {
        public void onSortComplete();
    }

    protected long id, playCount, createdAt, storageId;
    protected String title  = "";
    protected ArrayList<Track> allTracks = new ArrayList<>();
    protected ArrayList<Track> filteredTracks = new ArrayList<>();
    private long filterDuration = 60000;
    private Boolean shouldFilter = false;
    private String thumbImgUrl = "", fullImgUrl = "";
    private String thumbImgStoragePath = "", fullImgStoragePath = "";

    public CustomQ() {
    }

    public CustomQ(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public CustomQ(long id, String title, ArrayList<Track> tracks) {
        this.id = id;
        this.title = title;
        this.allTracks = tracks;
        refreshFilteredTracks(shouldFilter);
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPlayCount() {
        return playCount;
    }

    public void setPlayCount(long playCount) {
        this.playCount = playCount;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStorageId() {
        return storageId;
    }

    public void setStorageId(long storageId) {
        this.storageId = storageId;
    }

    public String getThumbImgUrl() {
        return thumbImgUrl;
    }

    public void setThumbImgUrl(String thumbImgUrl) {
        this.thumbImgUrl = thumbImgUrl;
    }

    public String getFullImgUrl() {
        return fullImgUrl;
    }

    public void setFullImgUrl(String fullImgUrl) {
        this.fullImgUrl = fullImgUrl;
    }

    public String getThumbImgStoragePath() {
        return thumbImgStoragePath;
    }

    public void setThumbImgStoragePath(String thumbImgStoragePath) {
        this.thumbImgStoragePath = thumbImgStoragePath;
    }

    public String getFullImgStoragePath() {
        return fullImgStoragePath;
    }

    public void setFullImgStoragePath(String fullImgStoragePath) {
        this.fullImgStoragePath = fullImgStoragePath;
    }

    public ArrayList<Track> getTracksList() {
        return filteredTracks;
    }

    public ArrayList<Track> getAllTracks() {
        return allTracks;
    }

    public void setTracksList(ArrayList<Track> tracks) {
        this.allTracks = tracks;
        refreshFilteredTracks(shouldFilter);
    }

    public void addTrack(Track track) {
        addTrack(track, false, true);
    }

    public void addTrackWithoutDuplicateCheck(Track track) {
        addTrack(track, false, false);
    }

    protected void addTrack(Track track, Boolean addInDb, Boolean checkDuplicate) {

        if (checkDuplicate) {
            if (allTracks.contains(track)) {
                return;
            }

            for (Track tr : allTracks) {
                if (tr.getTitle().toLowerCase().equals(track.getTitle().toLowerCase())) {
                    return;
                }
            }
        }

        allTracks.add(track);
        if (!shouldFilter || track.getDuration() > filterDuration) {
            filteredTracks.add(track);
        }
        if (addInDb) {
            DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).addTrack(this, track);
        }

    }

    public void addTracks(ArrayList<Track> tracks) {
        for(Track track : tracks) {
            addTrack(track, true, true);
        }
    }

    public Boolean containTrack(Track track) {
        return allTracks.contains(track);
    }

    public Boolean isTrackListEmpty() {
        return allTracks.size() == 0;
    }

    public void deleteTrack(Track track) {
        DatabaseHandler.getSharedInstance(MusicPlayer.getSharedInstance().getContext()).deleteTrackFromCustomQ(this, track);
        allTracks.remove(track);
        filteredTracks.remove(track);
    }

    public String toString() {
        return String.valueOf(id) + ": " + title + ":";
    }

    public void sortTracks(final String sortOrder, final OnSortCompleteCallback onSortCompleteCallback) {

        new AsyncTask<Object, Object, Object>() {

            @Override
            protected Object doInBackground(Object... params) {

                Log.e("joymix", "sort started");
                Comparator<Track> comparator = getComparator(sortOrder);
                try {
                    Collections.sort(allTracks, comparator);
                    Collections.sort(filteredTracks, comparator);
                }catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {

                super.onPostExecute(o);
                if (onSortCompleteCallback != null) {
                    onSortCompleteCallback.onSortComplete();
                }
                Log.e("joymix", "sort done");

            }
        }.execute();

    }

    private Comparator<Track> getComparator(String sortOrder) {

        Comparator<Track> comparator;
        if (sortOrder.equals(Constants.SORT_ORDER.A_TO_Z)) {
            comparator = new Comparator<Track>() {
                @Override
                public int compare(Track lhs, Track rhs) {
                    return lhs.getTitle().toLowerCase().compareTo(rhs.getTitle().toLowerCase());
                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.Z_TO_A)) {
            comparator = new Comparator<Track>() {
                @Override
                public int compare(Track lhs, Track rhs) {
                    return rhs.getTitle().toLowerCase().compareTo(lhs.getTitle().toLowerCase());
                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.CREATED_AT)) {
            comparator = new Comparator<Track>() {
                @Override
                public int compare(Track lhs, Track rhs) {

                    long l1 = lhs.getCreatedAt(), l2 = rhs.getCreatedAt();
                    return l1 > l2 ? 1: l1 < l2 ? -1 : 0;

                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.YEAR)) {
            comparator = new Comparator<Track>() {
                @Override
                public int compare(Track lhs, Track rhs) {

                    if (lhs.getYear() == 0 && rhs.getYear() == 0) {
                        return lhs.getTitle().toLowerCase().compareTo(rhs.getTitle().toLowerCase());
                    }
                    long l1 = lhs.getYear(), l2 = rhs.getYear();
                    return l1 > l2 ? 1: l1 < l2 ? -1 : 0;

                }
            };
        }else if (sortOrder.equals(Constants.SORT_ORDER.PLAY_COUNT)) {
            comparator = new Comparator<Track>() {
                @Override
                public int compare(Track lhs, Track rhs) {

                    long l1 = lhs.getPlayCount(), l2 = rhs.getPlayCount();
                    return l1 > l2 ? 1: l1 < l2 ? -1 : 0;

                }
            };
        }else {
            comparator = new Comparator<Track>() {
                @Override
                public int compare(Track lhs, Track rhs) {

                    long l1 = lhs.getId(), l2 = rhs.getId();
                    return l1 > l2 ? 1: l1 < l2 ? -1 : 0;

                }
            };
        }

        return comparator;
    }

    public void loadData(CustomQ customQ) {

        this.id = customQ.id;
        this.playCount = customQ.playCount;
        this.createdAt = customQ.createdAt;
        this.title = customQ.title;
        this.storageId = customQ.storageId;

        this.allTracks.clear();
        this.filteredTracks.clear();
        for (Track track : customQ.allTracks) {
            this.allTracks.add(track);
        }
        for (Track track : customQ.filteredTracks) {
            this.filteredTracks.add(track);
        }
        this.thumbImgUrl = customQ.thumbImgUrl;
        this.thumbImgStoragePath = customQ.thumbImgStoragePath;
        this.fullImgUrl = customQ.fullImgUrl;
        this.fullImgStoragePath = customQ.fullImgStoragePath;

    }

    private void filterTracks() {

        filteredTracks.clear();
        for(Track track : allTracks) {
            if (track.getDuration() > filterDuration) {
                filteredTracks.add(track);
            }
        }

    }

    public void refreshFilteredTracks(Boolean shouldFilter) {
        this.shouldFilter = shouldFilter;
        if (shouldFilter) {
            filterTracks();
        }else {
            filteredTracks.clear();
            for (Track track : allTracks) {
                filteredTracks.add(track);
            }
        }
    }

}
