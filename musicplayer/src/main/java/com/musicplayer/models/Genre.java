package com.musicplayer.models;

/**
 * Created by umair on 11/5/16.
 */
public class Genre extends CustomQ {

    public Genre() {
    }

    public Genre(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public void rename(String newTitle) {
        title = newTitle;
    }

}
