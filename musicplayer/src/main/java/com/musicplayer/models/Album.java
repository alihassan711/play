package com.musicplayer.models;

/**
 * Created by umair on 11/5/16.
 */
public class Album extends CustomQ {

    public Album() {
    }

    public Album(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public void rename(String newTitle) {
        title = newTitle;
    }

}
